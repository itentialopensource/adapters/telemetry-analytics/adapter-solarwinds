
## 0.6.1 [05-04-2023]

* Added new call to create custom property for orion node

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!16

---

## 0.6.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!15

---

## 0.5.10 [07-28-2021]

- Change the return data flag on a call

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!14

---

## 0.5.9 [06-28-2021]

- Change return data flags for 2 calls that do not have returned data

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!13

---

## 0.5.8 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!12

---

## 0.5.7 [02-24-2021]

- Migration to latest foundation and changes requested by SolarWinds

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!11

---

## 0.5.6 [12-22-2020]

- Changed the return Flag for BulkDelete Task to False.

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!10

---

## 0.5.5 [12-11-2020]

- Updated the DeleteUri returnFlag in adapter.js to False and updated the sampleProperties.json.

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!9

---

## 0.5.4 [07-10-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!6

---

## 0.5.3 [05-06-2020]

- Devel

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!5

---

## 0.5.2 [04-29-2020] & 0.5.1 [01-15-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!4

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!3

---

## 0.4.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-16-2019]

- Migrate the adapter to the new foundation, categorize it and make it available to app artifact.

See merge request itentialopensource/adapters/telemetry-analytics/adapter-solarwinds!1

---

## 0.1.1 [06-07-2019]

- Initial Commit

See commit 41a03f0

---
