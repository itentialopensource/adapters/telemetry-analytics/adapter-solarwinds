/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-solarwinds',
      type: 'Solarwinds',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Solarwinds = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] SolarWinds Adapter Test', () => {
  describe('Solarwinds Class Tests', () => {
    const a = new Solarwinds(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('solarwinds'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('solarwinds'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Solarwinds', pronghornDotJson.export);
          assert.equal('SolarWinds', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-solarwinds', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('solarwinds'));
          assert.equal('Solarwinds', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-solarwinds', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-solarwinds', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getQuery - errors', () => {
      it('should have a getQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getQuery(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-solarwinds-adapter-getQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postQuery - errors', () => {
      it('should have a postQuery function', (done) => {
        try {
          assert.equal(true, typeof a.postQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUri - errors', () => {
      it('should have a getUri function', (done) => {
        try {
          assert.equal(true, typeof a.getUri === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uri', (done) => {
        try {
          a.getUri(null, (data, error) => {
            try {
              const displayE = 'uri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-solarwinds-adapter-getUri', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUri - errors', () => {
      it('should have a postUri function', (done) => {
        try {
          assert.equal(true, typeof a.postUri === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uri', (done) => {
        try {
          a.postUri(null, null, (data, error) => {
            try {
              const displayE = 'uri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-solarwinds-adapter-postUri', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUri - errors', () => {
      it('should have a deleteUri function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUri === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uri', (done) => {
        try {
          a.deleteUri(null, (data, error) => {
            try {
              const displayE = 'uri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-solarwinds-adapter-deleteUri', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBulkUpdate - errors', () => {
      it('should have a postBulkUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postBulkUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBulkDelete - errors', () => {
      it('should have a postBulkDelete function', (done) => {
        try {
          assert.equal(true, typeof a.postBulkDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAgentManagementAgent - errors', () => {
      it('should have a postCreateOrionAgentManagementAgent function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAgentManagementAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAgentManagementAgentPlugin - errors', () => {
      it('should have a postCreateOrionAgentManagementAgentPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAgentManagementAgentPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAgentManagementProxy - errors', () => {
      it('should have a postCreateOrionAgentManagementProxy function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAgentManagementProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAPMNodeToNodeLink - errors', () => {
      it('should have a postCreateOrionAPMNodeToNodeLink function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAPMNodeToNodeLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAPMDependencyTcpStatistics - errors', () => {
      it('should have a postCreateOrionAPMDependencyTcpStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAPMDependencyTcpStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAPMApplicationTcpConnection - errors', () => {
      it('should have a postCreateOrionAPMApplicationTcpConnection function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAPMApplicationTcpConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCirrusIgnoredNodes - errors', () => {
      it('should have a postCreateCirrusIgnoredNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCirrusIgnoredNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMBaselines - errors', () => {
      it('should have a postCreateNCMBaselines function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMBaselines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMBaselineNodeMap - errors', () => {
      it('should have a postCreateNCMBaselineNodeMap function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMBaselineNodeMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMBaselineViolations - errors', () => {
      it('should have a postCreateNCMBaselineViolations function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMBaselineViolations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMFirmwareOperations - errors', () => {
      it('should have a postCreateNCMFirmwareOperations function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMFirmwareOperations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMFirmwareOperationNodes - errors', () => {
      it('should have a postCreateNCMFirmwareOperationNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMFirmwareOperationNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMFirmwareUpgradeImages - errors', () => {
      it('should have a postCreateNCMFirmwareUpgradeImages function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMFirmwareUpgradeImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateNCMFirmwareUpgradeMachineTypes - errors', () => {
      it('should have a postCreateNCMFirmwareUpgradeMachineTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateNCMFirmwareUpgradeMachineTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCliDeviceTemplates - errors', () => {
      it('should have a postCreateCliDeviceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCliDeviceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCliDeviceTemplatesNodes - errors', () => {
      it('should have a postCreateCliDeviceTemplatesNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCliDeviceTemplatesNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCliCliSessionSettings - errors', () => {
      it('should have a postCreateCliCliSessionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCliCliSessionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionCloudAccounts - errors', () => {
      it('should have a postCreateOrionCloudAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionCloudAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexSystemPolicy - errors', () => {
      it('should have a postCreateCortexSystemPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexSystemPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciApic - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciApic function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciApic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciApplicationProfile - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciApplicationProfile function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciApplicationProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciCiscoAciCredential - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciCiscoAciCredential function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciCiscoAciCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciEndpointGroup - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciEndpointGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciEndpointGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciFabric - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciFabric function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciFabric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciPhysicalEntity - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciPhysicalEntity function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciPhysicalEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCiscoAciTenant - errors', () => {
      it('should have a postCreateCortexOrionCiscoAciTenant function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCiscoAciTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCmanContainer - errors', () => {
      it('should have a postCreateCortexOrionCmanContainer function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCmanContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCmanContainerAgent - errors', () => {
      it('should have a postCreateCortexOrionCmanContainerAgent function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCmanContainerAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCmanContainerEnvironmentVariable - errors', () => {
      it('should have a postCreateCortexOrionCmanContainerEnvironmentVariable function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCmanContainerEnvironmentVariable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCmanContainerHost - errors', () => {
      it('should have a postCreateCortexOrionCmanContainerHost function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCmanContainerHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCmanContainerImage - errors', () => {
      it('should have a postCreateCortexOrionCmanContainerImage function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCmanContainerImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionSnmpCredentialV2 - errors', () => {
      it('should have a postCreateCortexOrionSnmpCredentialV2 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionSnmpCredentialV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionSnmpCredentialV3 - errors', () => {
      it('should have a postCreateCortexOrionSnmpCredentialV3 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionSnmpCredentialV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionWindowsCredential - errors', () => {
      it('should have a postCreateCortexOrionWindowsCredential function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionWindowsCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionInterface - errors', () => {
      it('should have a postCreateCortexOrionInterface function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionCpu - errors', () => {
      it('should have a postCreateCortexOrionCpu function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionCpu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionNode - errors', () => {
      it('should have a postCreateCortexOrionNode function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionPowerControlUnit - errors', () => {
      it('should have a postCreateCortexOrionPowerControlUnit function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionPowerControlUnit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationAlarm - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationCluster - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationCluster function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationDataCenter - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationDataCenter function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationDataCenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationDatastore - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationDatastore function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationDatastore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationHost - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationHost function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationPhysicalDisk - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationPhysicalDisk function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationPhysicalDisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationPollingTask - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationPollingTask function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationPollingTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationTriggeredAlarmState - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationTriggeredAlarmState function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationTriggeredAlarmState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVCenter - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVCenter function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVCenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVirtualMachine - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVirtualMachine function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVirtualMachine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVirtualMachineDisk - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVirtualMachineDisk function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVirtualMachineDisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVMwareCredential - errors', () => {
      it('should have a postCreateCortexOrionVMwareCredential function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVMwareCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVSan - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVSan function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVSan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVSanDiskGroup - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVSanDiskGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVSanDiskGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVSanHealthGroup - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVSanHealthGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVSanHealthGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVSanObjectSpaceSummary - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVSanObjectSpaceSummary function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVSanObjectSpaceSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVirtualizationVSanResyncInfo - errors', () => {
      it('should have a postCreateCortexOrionVirtualizationVSanResyncInfo function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVirtualizationVSanResyncInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionScsiInformation - errors', () => {
      it('should have a postCreateCortexOrionScsiInformation function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionScsiInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateCortexOrionVolume - errors', () => {
      it('should have a postCreateCortexOrionVolume function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateCortexOrionVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPADatabaseInstanceData - errors', () => {
      it('should have a postCreateOrionDPADatabaseInstanceData function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPADatabaseInstanceData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPADatabaseInstanceApplicationRelationship - errors', () => {
      it('should have a postCreateOrionDPADatabaseInstanceApplicationRelationship function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPADatabaseInstanceApplicationRelationship === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPADatabaseInstanceLun - errors', () => {
      it('should have a postCreateOrionDPADatabaseInstanceLun function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPADatabaseInstanceLun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPADpaServer - errors', () => {
      it('should have a postCreateOrionDPADpaServer function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPADpaServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIApplications - errors', () => {
      it('should have a postCreateOrionDPIApplications function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIApplicationAssignments - errors', () => {
      it('should have a postCreateOrionDPIApplicationAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIApplicationAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIApplicationSettings - errors', () => {
      it('should have a postCreateOrionDPIApplicationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIApplicationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIProbes - errors', () => {
      it('should have a postCreateOrionDPIProbes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIProbes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIProbeAssignments - errors', () => {
      it('should have a postCreateOrionDPIProbeAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIProbeAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIProbeSettings - errors', () => {
      it('should have a postCreateOrionDPIProbeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIProbeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDPIProbeProperties - errors', () => {
      it('should have a postCreateOrionDPIProbeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDPIProbeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionESIIncidentService - errors', () => {
      it('should have a postCreateOrionESIIncidentService function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionESIIncidentService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionHAPools - errors', () => {
      it('should have a postCreateOrionHAPools function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionHAPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionHAResourcesInstances - errors', () => {
      it('should have a postCreateOrionHAResourcesInstances function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionHAResourcesInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionHAPoolMemberInterfacesInfo - errors', () => {
      it('should have a postCreateOrionHAPoolMemberInterfacesInfo function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionHAPoolMemberInterfacesInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNPMInterfaces - errors', () => {
      it('should have a postCreateOrionNPMInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNPMInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateIPAMIPNode - errors', () => {
      it('should have a postCreateIPAMIPNode function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateIPAMIPNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateIPAMSubnet - errors', () => {
      it('should have a postCreateIPAMSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateIPAMSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateIPAMGroupsCustomProperties - errors', () => {
      it('should have a postCreateIPAMGroupsCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateIPAMGroupsCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateIPAMNodesCustomProperties - errors', () => {
      it('should have a postCreateIPAMNodesCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateIPAMNodesCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionLicensingLicenseFilters - errors', () => {
      it('should have a postCreateOrionLicensingLicenseFilters function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionLicensingLicenseFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathEndpointServices - errors', () => {
      it('should have a postCreateOrionNetPathEndpointServices function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathEndpointServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathProbes - errors', () => {
      it('should have a postCreateOrionNetPathProbes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathProbes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathEndpointServiceAssignments - errors', () => {
      it('should have a postCreateOrionNetPathEndpointServiceAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathEndpointServiceAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathEndpointServiceProperties - errors', () => {
      it('should have a postCreateOrionNetPathEndpointServiceProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathEndpointServiceProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathThresholds - errors', () => {
      it('should have a postCreateOrionNetPathThresholds function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathThresholds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetPathNetworks - errors', () => {
      it('should have a postCreateOrionNetPathNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetPathNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNPMCustomPollerAssignmentOnNode - errors', () => {
      it('should have a postCreateOrionNPMCustomPollerAssignmentOnNode function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNPMCustomPollerAssignmentOnNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNPMCustomPollerAssignmentOnInterface - errors', () => {
      it('should have a postCreateOrionNPMCustomPollerAssignmentOnInterface function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNPMCustomPollerAssignmentOnInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionOLMMessageSources - errors', () => {
      it('should have a postCreateOrionOLMMessageSources function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionOLMMessageSources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAlerts - errors', () => {
      it('should have a postCreateOrionAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAlertStatus - errors', () => {
      it('should have a postCreateOrionAlertStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAlertStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAlertConfigurations - errors', () => {
      it('should have a postCreateOrionAlertConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAlertConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAlertDefinitions - errors', () => {
      it('should have a postCreateOrionAlertDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAlertDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionActions - errors', () => {
      it('should have a postCreateOrionActions function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionActions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionActionsProperties - errors', () => {
      it('should have a postCreateOrionActionsProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionActionsProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionActionAssignmentProperties - errors', () => {
      it('should have a postCreateOrionActionAssignmentProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionActionAssignmentProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionActionsAssignments - errors', () => {
      it('should have a postCreateOrionActionsAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionActionsAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionActionSchedules - errors', () => {
      it('should have a postCreateOrionActionSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionActionSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAlertSchedules - errors', () => {
      it('should have a postCreateOrionAlertSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAlertSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDiscoveryLogs - errors', () => {
      it('should have a postCreateOrionDiscoveryLogs function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDiscoveryLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDependencies - errors', () => {
      it('should have a postCreateOrionDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionDeletedAutoDependencies - errors', () => {
      it('should have a postCreateOrionDeletedAutoDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionDeletedAutoDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionEvents - errors', () => {
      it('should have a postCreateOrionEvents function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionMapStudioFiles - errors', () => {
      it('should have a postCreateOrionMapStudioFiles function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionMapStudioFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNodeNotes - errors', () => {
      it('should have a postCreateOrionNodeNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNodeNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNodes - errors', () => {
      it('should have a postCreateOrionNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionPollers - errors', () => {
      it('should have a postCreateOrionPollers function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionPollers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionResourceProperties - errors', () => {
      it('should have a postCreateOrionResourceProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionResourceProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionResources - errors', () => {
      it('should have a postCreateOrionResources function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionVolumes - errors', () => {
      it('should have a postCreateOrionVolumes function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionVolumes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNodeSettings - errors', () => {
      it('should have a postCreateOrionNodeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNodeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionWorldMapPoint - errors', () => {
      it('should have a postCreateOrionWorldMapPoint function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionWorldMapPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionWorldMapPointLabel - errors', () => {
      it('should have a postCreateOrionWorldMapPointLabel function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionWorldMapPointLabel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNetObjectDowntime - errors', () => {
      it('should have a postCreateOrionNetObjectDowntime function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNetObjectDowntime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionForecastMetrics - errors', () => {
      it('should have a postCreateOrionForecastMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionForecastMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionForecastCapacitySettings - errors', () => {
      it('should have a postCreateOrionForecastCapacitySettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionForecastCapacitySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionMaintenancePlan - errors', () => {
      it('should have a postCreateOrionMaintenancePlan function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionMaintenancePlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionMaintenancePlanAssignment - errors', () => {
      it('should have a postCreateOrionMaintenancePlanAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionMaintenancePlanAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionWebFavoriteResource - errors', () => {
      it('should have a postCreateOrionWebFavoriteResource function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionWebFavoriteResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionWebUserWebView - errors', () => {
      it('should have a postCreateOrionWebUserWebView function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionWebUserWebView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionOrionServers - errors', () => {
      it('should have a postCreateOrionOrionServers function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionOrionServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionPerfStackProjects - errors', () => {
      it('should have a postCreateOrionPerfStackProjects function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionPerfStackProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionPerfStackStatisticsEntity - errors', () => {
      it('should have a postCreateOrionPerfStackStatisticsEntity function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionPerfStackStatisticsEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionRemotingRemoteExecutionPackage - errors', () => {
      it('should have a postCreateOrionRemotingRemoteExecutionPackage function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionRemotingRemoteExecutionPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSamAppOpticsIntegration - errors', () => {
      it('should have a postCreateOrionSamAppOpticsIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSamAppOpticsIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSamAppOpticsRegistrationCountry - errors', () => {
      it('should have a postCreateOrionSamAppOpticsRegistrationCountry function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSamAppOpticsRegistrationCountry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSamAppOpticsRegistrationState - errors', () => {
      it('should have a postCreateOrionSamAppOpticsRegistrationState function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSamAppOpticsRegistrationState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSamAppOpticsApplicationPool - errors', () => {
      it('should have a postCreateOrionSamAppOpticsApplicationPool function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSamAppOpticsApplicationPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMServerConfiguration - errors', () => {
      it('should have a postCreateOrionSCMServerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMServerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMProfiles - errors', () => {
      it('should have a postCreateOrionSCMProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMNodesProfiles - errors', () => {
      it('should have a postCreateOrionSCMNodesProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMNodesProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMNodesProfilesArchive - errors', () => {
      it('should have a postCreateOrionSCMNodesProfilesArchive function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMNodesProfilesArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMNodesProfilesHistory - errors', () => {
      it('should have a postCreateOrionSCMNodesProfilesHistory function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMNodesProfilesHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMBaseline - errors', () => {
      it('should have a postCreateOrionSCMBaseline function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMBaseline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMProfileElements - errors', () => {
      it('should have a postCreateOrionSCMProfileElements function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMProfileElements === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMResultsPolledElements - errors', () => {
      it('should have a postCreateOrionSCMResultsPolledElements function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMResultsPolledElements === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMResultsPolledElementDetails - errors', () => {
      it('should have a postCreateOrionSCMResultsPolledElementDetails function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMResultsPolledElementDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMResultsNodesPollingErrors - errors', () => {
      it('should have a postCreateOrionSCMResultsNodesPollingErrors function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMResultsNodesPollingErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMResultsPolledElementErrors - errors', () => {
      it('should have a postCreateOrionSCMResultsPolledElementErrors function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMResultsPolledElementErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMResultsElementErrors - errors', () => {
      it('should have a postCreateOrionSCMResultsElementErrors function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMResultsElementErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSCMPollEntries - errors', () => {
      it('should have a postCreateOrionSCMPollEntries function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSCMPollEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSettingOverride - errors', () => {
      it('should have a postCreateOrionSettingOverride function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSettingOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMAgents - errors', () => {
      it('should have a postCreateOrionSEUMAgents function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMAgents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMRecordings - errors', () => {
      it('should have a postCreateOrionSEUMRecordings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMRecordings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMRecordingCustomProperties - errors', () => {
      it('should have a postCreateOrionSEUMRecordingCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMRecordingCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMRecordingSteps - errors', () => {
      it('should have a postCreateOrionSEUMRecordingSteps function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMRecordingSteps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMTransactions - errors', () => {
      it('should have a postCreateOrionSEUMTransactions function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMTransactions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMTransactionCustomProperties - errors', () => {
      it('should have a postCreateOrionSEUMTransactionCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMTransactionCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMTransactionRunParameters - errors', () => {
      it('should have a postCreateOrionSEUMTransactionRunParameters function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMTransactionRunParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMTransactionSteps - errors', () => {
      it('should have a postCreateOrionSEUMTransactionSteps function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMTransactionSteps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSEUMSettings - errors', () => {
      it('should have a postCreateOrionSEUMSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSEUMSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionAutoDependencyRoot - errors', () => {
      it('should have a postCreateOrionAutoDependencyRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionAutoDependencyRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionSSHSession - errors', () => {
      it('should have a postCreateOrionSSHSession function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionSSHSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionVIMClustersCustomProperties - errors', () => {
      it('should have a postCreateOrionVIMClustersCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionVIMClustersCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionVIMVirtualMachinesCustomProperties - errors', () => {
      it('should have a postCreateOrionVIMVirtualMachinesCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionVIMVirtualMachinesCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionVIMHostsCustomProperties - errors', () => {
      it('should have a postCreateOrionVIMHostsCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionVIMHostsCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionVIMDatastoresCustomProperties - errors', () => {
      it('should have a postCreateOrionVIMDatastoresCustomProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionVIMDatastoresCustomProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionWirelessHeatMapMap - errors', () => {
      it('should have a postCreateOrionWirelessHeatMapMap function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionWirelessHeatMapMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateSWISfRemoteSWIS - errors', () => {
      it('should have a postCreateSWISfRemoteSWIS function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateSWISfRemoteSWIS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateSystemSubscription - errors', () => {
      it('should have a postCreateSystemSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateSystemSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateSystemSubscriptionProperty - errors', () => {
      it('should have a postCreateSystemSubscriptionProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateSystemSubscriptionProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventoryPollNow - errors', () => {
      it('should have a postInvokeOrionADMNodeInventoryPollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventoryPollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventoryEnable - errors', () => {
      it('should have a postInvokeOrionADMNodeInventoryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventoryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventoryDisable - errors', () => {
      it('should have a postInvokeOrionADMNodeInventoryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventoryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventorySchedulePollNow - errors', () => {
      it('should have a postInvokeOrionADMNodeInventorySchedulePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventorySchedulePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventoryScheduleEnable - errors', () => {
      it('should have a postInvokeOrionADMNodeInventoryScheduleEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventoryScheduleEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionADMNodeInventoryUninstallConnectionQualityAgentPlugin - errors', () => {
      it('should have a postInvokeOrionADMNodeInventoryUninstallConnectionQualityAgentPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionADMNodeInventoryUninstallConnectionQualityAgentPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentDeploy - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentDeployToNode - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentDeployToNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentDeployToNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentDeployPlugin - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentDeployPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentDeployPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentRedeployPlugin - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentRedeployPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentRedeployPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentUninstallPlugin - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentUninstallPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentUninstallPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentUninstall - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentUninstall function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentUninstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentDelete - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentDelete function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentApproveReboot - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentApproveReboot function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentApproveReboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentApproveUpdate - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentApproveUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentApproveUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentTestWithEngine - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentTestWithEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentTestWithEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentAssignToEngine - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentValidateDeploymentCredentials - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentValidateDeploymentCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentValidateDeploymentCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAgentManagementAgentRestartAgent - errors', () => {
      it('should have a postInvokeOrionAgentManagementAgentRestartAgent function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAgentManagementAgentRestartAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISSiteStart - errors', () => {
      it('should have a postInvokeOrionAPMIISSiteStart function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISSiteStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISSiteStop - errors', () => {
      it('should have a postInvokeOrionAPMIISSiteStop function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISSiteStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISSiteRestart - errors', () => {
      it('should have a postInvokeOrionAPMIISSiteRestart function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISSiteRestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISApplicationPoolStart - errors', () => {
      it('should have a postInvokeOrionAPMIISApplicationPoolStart function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISApplicationPoolStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISApplicationPoolStop - errors', () => {
      it('should have a postInvokeOrionAPMIISApplicationPoolStop function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISApplicationPoolStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMIISApplicationPoolRestart - errors', () => {
      it('should have a postInvokeOrionAPMIISApplicationPoolRestart function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMIISApplicationPoolRestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationUnmanage - errors', () => {
      it('should have a postInvokeOrionAPMApplicationUnmanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationUnmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationRemanage - errors', () => {
      it('should have a postInvokeOrionAPMApplicationRemanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationRemanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationCreateApplication - errors', () => {
      it('should have a postInvokeOrionAPMApplicationCreateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationCreateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationDeleteApplication - errors', () => {
      it('should have a postInvokeOrionAPMApplicationDeleteApplication function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationDeleteApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationPollNow - errors', () => {
      it('should have a postInvokeOrionAPMApplicationPollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationPollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionAPMApplicationCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionAPMApplicationCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionAPMApplicationCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionAPMApplicationCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationTemplateUpdateApplicationTemplateSettings - errors', () => {
      it('should have a postInvokeOrionAPMApplicationTemplateUpdateApplicationTemplateSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationTemplateUpdateApplicationTemplateSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMApplicationTemplateDeleteTemplate - errors', () => {
      it('should have a postInvokeOrionAPMApplicationTemplateDeleteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMApplicationTemplateDeleteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMServerManagementStartService - errors', () => {
      it('should have a postInvokeOrionAPMServerManagementStartService function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMServerManagementStartService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMServerManagementStopService - errors', () => {
      it('should have a postInvokeOrionAPMServerManagementStopService function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMServerManagementStopService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMServerManagementRestartService - errors', () => {
      it('should have a postInvokeOrionAPMServerManagementRestartService function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMServerManagementRestartService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAPMServerManagementRebootNode - errors', () => {
      it('should have a postInvokeOrionAPMServerManagementRebootNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAPMServerManagementRebootNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionStacksRelationTraverse - errors', () => {
      it('should have a postInvokeOrionStacksRelationTraverse function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionStacksRelationTraverse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionStacksRelationProcessUi - errors', () => {
      it('should have a postInvokeOrionStacksRelationProcessUi function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionStacksRelationProcessUi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionASANodeExecuteCliCommand - errors', () => {
      it('should have a postInvokeOrionASANodeExecuteCliCommand function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionASANodeExecuteCliCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionASAInterfacesSetFavorite - errors', () => {
      it('should have a postInvokeOrionASAInterfacesSetFavorite function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionASAInterfacesSetFavorite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionASAInterfacesRemoveFavorite - errors', () => {
      it('should have a postInvokeOrionASAInterfacesRemoveFavorite function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionASAInterfacesRemoveFavorite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsGetJob - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsGetJob function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsGetJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsUpdateJob - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsUpdateJob function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsUpdateJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsAddJob - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsAddJob function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsAddJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsDeleteJobs - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsDeleteJobs function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsDeleteJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsEnableOrDisableJobs - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsEnableOrDisableJobs function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsEnableOrDisableJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsClearJobLog - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsClearJobLog function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsClearJobLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsGetJobStatus - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsGetJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsGetJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusNCMNCMJobsGetJobLog - errors', () => {
      it('should have a postInvokeCirrusNCMNCMJobsGetJobLog function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusNCMNCMJobsGetJobLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusInventoryQueueStartInventory - errors', () => {
      it('should have a postInvokeCirrusInventoryQueueStartInventory function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusInventoryQueueStartInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusInventoryQueueReportError - errors', () => {
      it('should have a postInvokeCirrusInventoryQueueReportError function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusInventoryQueueReportError === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusInventoryQueueCancelInventory - errors', () => {
      it('should have a postInvokeCirrusInventoryQueueCancelInventory function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusInventoryQueueCancelInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusInventoryQueueClearInventory - errors', () => {
      it('should have a postInvokeCirrusInventoryQueueClearInventory function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusInventoryQueueClearInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMVulnerabilitiesAnnouncementsStartVulnerabilityMatching - errors', () => {
      it('should have a postInvokeNCMVulnerabilitiesAnnouncementsStartVulnerabilityMatching function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMVulnerabilitiesAnnouncementsStartVulnerabilityMatching === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMVulnerabilitiesAnnouncementsIsVulnerabilityMatchingActive - errors', () => {
      it('should have a postInvokeNCMVulnerabilitiesAnnouncementsIsVulnerabilityMatchingActive function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMVulnerabilitiesAnnouncementsIsVulnerabilityMatchingActive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMVulnerabilitiesAnnouncementsInitVulnerabilitySchedule - errors', () => {
      it('should have a postInvokeNCMVulnerabilitiesAnnouncementsInitVulnerabilitySchedule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMVulnerabilitiesAnnouncementsInitVulnerabilitySchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsAddSnippet - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsAddSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsAddSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsGetSnippet - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsGetSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsGetSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsUpdateSnippet - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsUpdateSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsUpdateSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsDeleteSnippets - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsDeleteSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsDeleteSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsImportSnippets - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsImportSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsImportSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsCopySnippets - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsCopySnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsCopySnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsGetTagsList - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsGetTagsList function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsGetTagsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsGetTagsListForSnippets - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsGetTagsListForSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsGetTagsListForSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsAddTags - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsAddTags function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsAddTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsDeleteTags - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsDeleteTags function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsDeleteTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigSnippetsSaveSnippetAsCopy - errors', () => {
      it('should have a postInvokeCirrusConfigSnippetsSaveSnippetAsCopy function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigSnippetsSaveSnippetAsCopy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveDownload - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveDownload function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveGetStatus - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveGetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveGetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveGetStatusWithLimitations - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveGetStatusWithLimitations function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveGetStatusWithLimitations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveUpload - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveUpload function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveExecute - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveExecute function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveExecute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveCancelTransfer - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveCancelTransfer function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveCancelTransfer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveCancelTransferWithLimitations - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveCancelTransferWithLimitations function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveCancelTransferWithLimitations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearComplete - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearComplete function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearComplete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearCompleteWithLimitations - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearCompleteWithLimitations function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearCompleteWithLimitations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearErrors - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearErrors function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearErrorsWithLimitations - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearErrorsWithLimitations function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearErrorsWithLimitations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearAll - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearAll function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearAllWithLimitations - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearAllWithLimitations function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearAllWithLimitations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveGetPermissionsByRole - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveGetPermissionsByRole function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveGetPermissionsByRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveDeleteConfigs - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveDeleteConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveDeleteConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveGetConfigTypes - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveGetConfigTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveGetConfigTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveReportError - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveReportError function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveReportError === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveCancelTransfers - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveCancelTransfers function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveCancelTransfers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveClearTransfers - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveClearTransfers function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveClearTransfers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveExecuteScript - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveExecuteScript function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveExecuteScript === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveUploadConfig - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveUploadConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveUploadConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveDownloadConfig - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveDownloadConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveDownloadConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveExecuteScriptOnNodes - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveExecuteScriptOnNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveExecuteScriptOnNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveDownloadConfigOnNodes - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveDownloadConfigOnNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveDownloadConfigOnNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveConfigSearch - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveConfigSearch function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveConfigSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveRunIndexOptimization - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveRunIndexOptimization function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveRunIndexOptimization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveUpdateConfig - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveUpdateConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveUpdateConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveCloneConfig - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveCloneConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveCloneConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveImportConfig - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveImportConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveImportConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveSetClearBaseline - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveSetClearBaseline function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveSetClearBaseline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveValidateBinaryConfigStorage - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveValidateBinaryConfigStorage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveValidateBinaryConfigStorage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveSearchInterfaceConfigSnippet - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveSearchInterfaceConfigSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveSearchInterfaceConfigSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusConfigArchiveGetInterfaceConfigSnippets - errors', () => {
      it('should have a postInvokeCirrusConfigArchiveGetInterfaceConfigSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusConfigArchiveGetInterfaceConfigSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusSnippetArchiveAddSnippet - errors', () => {
      it('should have a postInvokeCirrusSnippetArchiveAddSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusSnippetArchiveAddSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusSnippetArchiveDeleteSnippet - errors', () => {
      it('should have a postInvokeCirrusSnippetArchiveDeleteSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusSnippetArchiveDeleteSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusSnippetArchiveUpdateSnippet - errors', () => {
      it('should have a postInvokeCirrusSnippetArchiveUpdateSnippet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusSnippetArchiveUpdateSnippet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsTestRule - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsTestRule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsTestRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPolicyReport - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPolicyReport function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPolicyReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsUpdatePolicyReport - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsUpdatePolicyReport function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsUpdatePolicyReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsAddPolicyReport - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsAddPolicyReport function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsAddPolicyReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsDeletePolicyReports - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsDeletePolicyReports function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsDeletePolicyReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPolicy - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsUpdatePolicy - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsUpdatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsUpdatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsAddPolicy - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsAddPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsAddPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsDeletePolicies - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsDeletePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsDeletePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPolicyRule - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsUpdatePolicyRule - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsUpdatePolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsUpdatePolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsAddPolicyRule - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsAddPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsAddPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsDeletePolicyRules - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsDeletePolicyRules function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsDeletePolicyRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsStartCaching - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsStartCaching function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsStartCaching === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsUpdateReportStatus - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsUpdateReportStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsUpdateReportStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPagablePoliciesList - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPagablePoliciesList function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPagablePoliciesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPoliciesRowCount - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPoliciesRowCount function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPoliciesRowCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPagablePolicyRulesList - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPagablePolicyRulesList function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPagablePolicyRulesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetPolicyRulesRowCount - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetPolicyRulesRowCount function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetPolicyRulesRowCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetComplianceDataTable - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetComplianceDataTable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetComplianceDataTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetComplianceColumnsInJSON - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetComplianceColumnsInJSON function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetComplianceColumnsInJSON === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGetRowNumber - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGetRowNumber function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGetRowNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusPolicyReportsGenerateRemediationScriptForNodes - errors', () => {
      it('should have a postInvokeCirrusPolicyReportsGenerateRemediationScriptForNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusPolicyReportsGenerateRemediationScriptForNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMEosBeginRefreshAll - errors', () => {
      it('should have a postInvokeNCMEosBeginRefreshAll function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMEosBeginRefreshAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMEosRefreshNow - errors', () => {
      it('should have a postInvokeNCMEosRefreshNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMEosRefreshNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMEosIsRefreshingAll - errors', () => {
      it('should have a postInvokeNCMEosIsRefreshingAll function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMEosIsRefreshingAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMEosInitSchedule - errors', () => {
      it('should have a postInvokeNCMEosInitSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMEosInitSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareStorageValidateFirmwareStorage - errors', () => {
      it('should have a postInvokeNCMFirmwareStorageValidateFirmwareStorage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareStorageValidateFirmwareStorage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareStorageDeleteFirmwareImages - errors', () => {
      it('should have a postInvokeNCMFirmwareStorageDeleteFirmwareImages function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareStorageDeleteFirmwareImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareDefinitionsGetFirmwareDefinition - errors', () => {
      it('should have a postInvokeNCMFirmwareDefinitionsGetFirmwareDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareDefinitionsGetFirmwareDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareDefinitionsAddFirmwareDefinition - errors', () => {
      it('should have a postInvokeNCMFirmwareDefinitionsAddFirmwareDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareDefinitionsAddFirmwareDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareDefinitionsUpdateFirmwareDefinition - errors', () => {
      it('should have a postInvokeNCMFirmwareDefinitionsUpdateFirmwareDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareDefinitionsUpdateFirmwareDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareDefinitionsDeleteFirmwareDefinitions - errors', () => {
      it('should have a postInvokeNCMFirmwareDefinitionsDeleteFirmwareDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareDefinitionsDeleteFirmwareDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsPrepareFirmwareUpgrade - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsPrepareFirmwareUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsPrepareFirmwareUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsPrepareRollBack - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsPrepareRollBack function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsPrepareRollBack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsPrepareReExecuteFailed - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsPrepareReExecuteFailed function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsPrepareReExecuteFailed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsStartUpgrade - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsStartUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsStartUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsCancelUpgrade - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsCancelUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsCancelUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeNCMFirmwareOperationsGenerateScriptPreview - errors', () => {
      it('should have a postInvokeNCMFirmwareOperationsGenerateScriptPreview function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeNCMFirmwareOperationsGenerateScriptPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusReportsRun - errors', () => {
      it('should have a postInvokeCirrusReportsRun function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusReportsRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusReportsRunPagable - errors', () => {
      it('should have a postInvokeCirrusReportsRunPagable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusReportsRunPagable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusReportsGetRecordsCount - errors', () => {
      it('should have a postInvokeCirrusReportsGetRecordsCount function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusReportsGetRecordsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCirrusReportsGetDiplayableColumns - errors', () => {
      it('should have a postInvokeCirrusReportsGetDiplayableColumns function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCirrusReportsGetDiplayableColumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionGroupCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionGroupCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionGroupCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionGroupCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionGroupCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionGroupCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionGroupCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionGroupCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionGroupCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionGroupCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionGroupCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionGroupCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionGroupCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionGroupCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionGroupCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionContainerMemberDefinitionGetMembers - errors', () => {
      it('should have a postInvokeOrionContainerMemberDefinitionGetMembers function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionContainerMemberDefinitionGetMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionContainerMemberDefinitionGetFirstNMembers - errors', () => {
      it('should have a postInvokeOrionContainerMemberDefinitionGetFirstNMembers function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionContainerMemberDefinitionGetFirstNMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexManagementPublishEvent - errors', () => {
      it('should have a postInvokeCortexManagementPublishEvent function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexManagementPublishEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciAssignAciPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciAssignAciPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciAssignAciPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciGetPollInterval - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciGetPollInterval function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciGetPollInterval === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciIsAciPollingAssigned - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciIsAciPollingAssigned function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciIsAciPollingAssigned === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciSyncAciCredentialsWithOrion - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciSyncAciCredentialsWithOrion function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciSyncAciCredentialsWithOrion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciTestAciCredentials - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciTestAciCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciTestAciCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApicOrionCiscoAciUnassignAciPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApicOrionCiscoAciUnassignAciPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApicOrionCiscoAciUnassignAciPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciApplicationProfileCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciApplicationProfileCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciApplicationProfileCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciEndpointGroupCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciEndpointGroupCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciEndpointGroupCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciFabricCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciFabricCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciFabricCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciPhysicalEntityCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciPhysicalEntityCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciPhysicalEntityCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCiscoAciTenantCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCiscoAciTenantCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCiscoAciTenantCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerAgentCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerAgentCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerAgentCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerHostCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerHostCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerHostCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionCmanContainerImageCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionCmanContainerImageCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionCmanContainerImageCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionMonitoringElementCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionMonitoringElementCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionMonitoringElementCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreAddToCortex - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreAddToCortex function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreAddToCortex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionInterfaceCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionInterfaceCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionInterfaceCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreAddToCortex - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreAddToCortex function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreAddToCortex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionNodeCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionNodeCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionNodeCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionNodeCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionPowerControlUnitCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionPowerControlUnitCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionPowerControlUnitCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationClusterCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationClusterCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationClusterCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDataCenterCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDataCenterCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDataCenterCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationDatastoreCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationDatastoreCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationDatastoreCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHostCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHostCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHostCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationHypervisorEntityCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationHypervisorEntityCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationHypervisorEntityCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVCenterCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVCenterCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVCenterCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVirtualizationVSanCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVirtualizationVSanCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVirtualizationVSanCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreAddToCortex - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreAddToCortex function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreAddToCortex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreAssignToEngine - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreAssignToEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreAssignToEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreGetSupportedMetrics - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreGetSupportedMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreGetSupportedMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreInventoryNow - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreInventoryNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreInventoryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCorePollNow - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCorePollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCorePollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreSetPolling - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreSetPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreSetPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreStartRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreStartRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreStartRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeCortexOrionVolumeCoreStopRealTimePolling - errors', () => {
      it('should have a postInvokeCortexOrionVolumeCoreStopRealTimePolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeCortexOrionVolumeCoreStopRealTimePolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPADpaServerRefreshSchema - errors', () => {
      it('should have a postInvokeOrionDPADpaServerRefreshSchema function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPADpaServerRefreshSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPIProbesReloadProbeSettings - errors', () => {
      it('should have a postInvokeOrionDPIProbesReloadProbeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPIProbesReloadProbeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPIProbesReloadAppDefinitions - errors', () => {
      it('should have a postInvokeOrionDPIProbesReloadAppDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPIProbesReloadAppDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPIProbesGetProbeCapabilities - errors', () => {
      it('should have a postInvokeOrionDPIProbesGetProbeCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPIProbesGetProbeCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPIProbesDeployLocalTrafficProbe - errors', () => {
      it('should have a postInvokeOrionDPIProbesDeployLocalTrafficProbe function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPIProbesDeployLocalTrafficProbe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDPIProbesDeploySpanPortProbe - errors', () => {
      it('should have a postInvokeOrionDPIProbesDeploySpanPortProbe function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDPIProbesDeploySpanPortProbe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionESIIncidentIntegrationSetIncidentIntegrationState - errors', () => {
      it('should have a postInvokeOrionESIIncidentIntegrationSetIncidentIntegrationState function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionESIIncidentIntegrationSetIncidentIntegrationState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionF5SystemDeviceTestApiPolling - errors', () => {
      it('should have a postInvokeOrionF5SystemDeviceTestApiPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionF5SystemDeviceTestApiPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionF5SystemDeviceEnableApiPolling - errors', () => {
      it('should have a postInvokeOrionF5SystemDeviceEnableApiPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionF5SystemDeviceEnableApiPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionF5SystemDeviceDisableApiPolling - errors', () => {
      it('should have a postInvokeOrionF5SystemDeviceDisableApiPolling function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionF5SystemDeviceDisableApiPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionF5LTMServerLinkNode - errors', () => {
      it('should have a postInvokeOrionF5LTMServerLinkNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionF5LTMServerLinkNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionF5LTMServerUnlinkNode - errors', () => {
      it('should have a postInvokeOrionF5LTMServerUnlinkNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionF5LTMServerUnlinkNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareInfoBaseEnableHardwareHealth - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareInfoBaseEnableHardwareHealth function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareInfoBaseEnableHardwareHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareInfoBaseDisableHardwareHealth - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareInfoBaseDisableHardwareHealth function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareInfoBaseDisableHardwareHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareInfoBaseDeleteHardwareHealth - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareInfoBaseDeleteHardwareHealth function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareInfoBaseDeleteHardwareHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareInfoBaseIsHardwareHealthEnabled - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareInfoBaseIsHardwareHealthEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareInfoBaseIsHardwareHealthEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareItemBaseEnableSensors - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareItemBaseEnableSensors function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareItemBaseEnableSensors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareItemBaseDisableSensors - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareItemBaseDisableSensors function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareItemBaseDisableSensors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareItemThresholdSetThreshold - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareItemThresholdSetThreshold function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareItemThresholdSetThreshold === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHardwareHealthHardwareItemThresholdClearThresholds - errors', () => {
      it('should have a postInvokeOrionHardwareHealthHardwareItemThresholdClearThresholds function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHardwareHealthHardwareItemThresholdClearThresholds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsCreatePool - errors', () => {
      it('should have a postInvokeOrionHAPoolsCreatePool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsCreatePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsEditPool - errors', () => {
      it('should have a postInvokeOrionHAPoolsEditPool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsEditPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsValidateCreatePool - errors', () => {
      it('should have a postInvokeOrionHAPoolsValidateCreatePool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsValidateCreatePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsValidateEditPool - errors', () => {
      it('should have a postInvokeOrionHAPoolsValidateEditPool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsValidateEditPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsEnablePool - errors', () => {
      it('should have a postInvokeOrionHAPoolsEnablePool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsEnablePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsDisablePool - errors', () => {
      it('should have a postInvokeOrionHAPoolsDisablePool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsDisablePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsDeletePool - errors', () => {
      it('should have a postInvokeOrionHAPoolsDeletePool function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsDeletePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsSwitchover - errors', () => {
      it('should have a postInvokeOrionHAPoolsSwitchover function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsSwitchover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionHAPoolsDeleteStaleEngine - errors', () => {
      it('should have a postInvokeOrionHAPoolsDeleteStaleEngine function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionHAPoolsDeleteStaleEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesSetPowerLevel - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesSetPowerLevel function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesSetPowerLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesUnmanage - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesUnmanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesUnmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesRemanage - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesRemanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesRemanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesDiscoverInterfacesOnNode - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesDiscoverInterfacesOnNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesDiscoverInterfacesOnNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesAddInterfacesOnNode - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesAddInterfacesOnNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesAddInterfacesOnNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesCreateInterfacesPluginConfiguration - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesCreateInterfacesPluginConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesCreateInterfacesPluginConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNPMInterfacesCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionNPMInterfacesCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNPMInterfacesCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementChangeIpStatus - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementChangeIpStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementChangeIpStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementChangeIpStatusForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementChangeIpStatusForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementChangeIpStatusForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementGetFirstAvailableIp - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementGetFirstAvailableIp function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementGetFirstAvailableIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementGetFirstAvailableIpForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementGetFirstAvailableIpForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementGetFirstAvailableIpForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementStartIpReservation - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementStartIpReservation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementStartIpReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementStartIpReservationForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementStartIpReservationForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementStartIpReservationForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCancelIpReservation - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCancelIpReservation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCancelIpReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCancelIpReservationForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCancelIpReservationForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCancelIpReservationForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementFinishIpReservation - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementFinishIpReservation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementFinishIpReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementFinishIpReservationForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementFinishIpReservationForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementFinishIpReservationForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCreateSubnet - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCreateSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCreateSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCreateSubnetForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCreateSubnetForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCreateSubnetForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCreateIPv6Subnet - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCreateIPv6Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCreateIPv6Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementCreateIPv6SubnetForGroup - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementCreateIPv6SubnetForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementCreateIPv6SubnetForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMSubnetManagementChangeDisableAutoScanning - errors', () => {
      it('should have a postInvokeIPAMSubnetManagementChangeDisableAutoScanning function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMSubnetManagementChangeDisableAutoScanning === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMDhcpDnsManagementCreateIpReservation - errors', () => {
      it('should have a postInvokeIPAMDhcpDnsManagementCreateIpReservation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMDhcpDnsManagementCreateIpReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMDhcpDnsManagementRemoveIpReservation - errors', () => {
      it('should have a postInvokeIPAMDhcpDnsManagementRemoveIpReservation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMDhcpDnsManagementRemoveIpReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMDhcpDnsManagementGetAandPTRrecordsForDnsZone - errors', () => {
      it('should have a postInvokeIPAMDhcpDnsManagementGetAandPTRrecordsForDnsZone function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMDhcpDnsManagementGetAandPTRrecordsForDnsZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementAddDnsARecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementAddDnsARecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementAddDnsARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementChangeDnsARecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementChangeDnsARecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementChangeDnsARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementRemoveDnsARecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementRemoveDnsARecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementRemoveDnsARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementAddDnsAaaaRecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementAddDnsAaaaRecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementAddDnsAaaaRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementChangeDnsAaaaRecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementChangeDnsAaaaRecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementChangeDnsAaaaRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementRemoveDnsAaaaRecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementRemoveDnsAaaaRecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementRemoveDnsAaaaRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementAddDnsARecordWithPtr - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementAddDnsARecordWithPtr function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementAddDnsARecordWithPtr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementAddPtrToDnsARecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementAddPtrToDnsARecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementAddPtrToDnsARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementAddPtrRecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementAddPtrRecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementAddPtrRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMIPAddressManagementRemovePtrRecord - errors', () => {
      it('should have a postInvokeIPAMIPAddressManagementRemovePtrRecord function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMIPAddressManagementRemovePtrRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMAttrDefineAddCustomProperty - errors', () => {
      it('should have a postInvokeIPAMAttrDefineAddCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMAttrDefineAddCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMAttrDefineUpdateCustomProperty - errors', () => {
      it('should have a postInvokeIPAMAttrDefineUpdateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMAttrDefineUpdateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMAttrDefineDeleteCustomProperty - errors', () => {
      it('should have a postInvokeIPAMAttrDefineDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMAttrDefineDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMGroupsCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeIPAMGroupsCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMGroupsCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMGroupsCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeIPAMGroupsCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMGroupsCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMGroupsCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeIPAMGroupsCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMGroupsCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMGroupsCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeIPAMGroupsCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMGroupsCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMNodesCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeIPAMNodesCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMNodesCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMNodesCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeIPAMNodesCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMNodesCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMNodesCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeIPAMNodesCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMNodesCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeIPAMNodesCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeIPAMNodesCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeIPAMNodesCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesActivateOnline - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesActivateOnline function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesActivateOnline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesActivateOffline - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesActivateOffline function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesActivateOffline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesDeactivate - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesDeactivate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesDeactivate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesReAssignExactlyTo - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesReAssignExactlyTo function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesReAssignExactlyTo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesUnAssignFromAllServers - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesUnAssignFromAllServers function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesUnAssignFromAllServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesFindValidAssignments - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesFindValidAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesFindValidAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesGetEvaluationState - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesGetEvaluationState function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesGetEvaluationState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionLicensingLicensesGetAvailableAssignments - errors', () => {
      it('should have a postInvokeOrionLicensingLicensesGetAvailableAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionLicensingLicensesGetAvailableAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNetflowSourceSetManualSamplingRate - errors', () => {
      it('should have a postInvokeOrionNetflowSourceSetManualSamplingRate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNetflowSourceSetManualSamplingRate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNetflowSourceSetAutoDetectedSamplingRate - errors', () => {
      it('should have a postInvokeOrionNetflowSourceSetAutoDetectedSamplingRate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNetflowSourceSetAutoDetectedSamplingRate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionOLMLogEntryUidMaxForDate - errors', () => {
      it('should have a postInvokeOrionOLMLogEntryUidMaxForDate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionOLMLogEntryUidMaxForDate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionOLMLogEntryUidMinForDate - errors', () => {
      it('should have a postInvokeOrionOLMLogEntryUidMinForDate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionOLMLogEntryUidMinForDate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionOLMLogEntryUidExtractDate - errors', () => {
      it('should have a postInvokeOrionOLMLogEntryUidExtractDate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionOLMLogEntryUidExtractDate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertActiveAcknowledge - errors', () => {
      it('should have a postInvokeOrionAlertActiveAcknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertActiveAcknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertActiveUnacknowledge - errors', () => {
      it('should have a postInvokeOrionAlertActiveUnacknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertActiveUnacknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertActiveClearAlert - errors', () => {
      it('should have a postInvokeOrionAlertActiveClearAlert function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertActiveClearAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertActiveAppendNote - errors', () => {
      it('should have a postInvokeOrionAlertActiveAppendNote function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertActiveAppendNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertsMigrateAllBasicAlerts - errors', () => {
      it('should have a postInvokeOrionAlertsMigrateAllBasicAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertsMigrateAllBasicAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertsMigrateBasicAlert - errors', () => {
      it('should have a postInvokeOrionAlertsMigrateBasicAlert function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertsMigrateBasicAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertStatusAcknowledge - errors', () => {
      it('should have a postInvokeOrionAlertStatusAcknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertStatusAcknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertStatusAcknowledgeAlert - errors', () => {
      it('should have a postInvokeOrionAlertStatusAcknowledgeAlert function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertStatusAcknowledgeAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertStatusAddNote - errors', () => {
      it('should have a postInvokeOrionAlertStatusAddNote function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertStatusAddNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsMigrateAllAdvancedAlerts - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsMigrateAllAdvancedAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsMigrateAllAdvancedAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsMigrateAdvancedAlert - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsMigrateAdvancedAlert function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsMigrateAdvancedAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsMigrateAdvancedAlertFromXML - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsMigrateAdvancedAlertFromXML function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsMigrateAdvancedAlertFromXML === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsImport - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsImport function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsExport - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsExport function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertConfigurationsCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionAlertConfigurationsCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertConfigurationsCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertDefinitionsRevertMigratedAlert - errors', () => {
      it('should have a postInvokeOrionAlertDefinitionsRevertMigratedAlert function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertDefinitionsRevertMigratedAlert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsDeleteActionsByAssignments - errors', () => {
      it('should have a postInvokeOrionActionsDeleteActionsByAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsDeleteActionsByAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsDeleteActionsByAssignmentsAndCategory - errors', () => {
      it('should have a postInvokeOrionActionsDeleteActionsByAssignmentsAndCategory function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsDeleteActionsByAssignmentsAndCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsSaveActionsForAssignments - errors', () => {
      it('should have a postInvokeOrionActionsSaveActionsForAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsSaveActionsForAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsUpdateAction - errors', () => {
      it('should have a postInvokeOrionActionsUpdateAction function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsUpdateAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsUpdateActionsProperties - errors', () => {
      it('should have a postInvokeOrionActionsUpdateActionsProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsUpdateActionsProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsUpdateActionsDescriptions - errors', () => {
      it('should have a postInvokeOrionActionsUpdateActionsDescriptions function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsUpdateActionsDescriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsUpdateActionsFrequencies - errors', () => {
      it('should have a postInvokeOrionActionsUpdateActionsFrequencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsUpdateActionsFrequencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsTestAlertingAction - errors', () => {
      it('should have a postInvokeOrionActionsTestAlertingAction function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsTestAlertingAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionActionsTestReportingAction - errors', () => {
      it('should have a postInvokeOrionActionsTestReportingAction function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionActionsTestReportingAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertSuppressionSuppressAlerts - errors', () => {
      it('should have a postInvokeOrionAlertSuppressionSuppressAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertSuppressionSuppressAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertSuppressionResumeAlerts - errors', () => {
      it('should have a postInvokeOrionAlertSuppressionResumeAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertSuppressionResumeAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionAlertSuppressionGetAlertSuppressionState - errors', () => {
      it('should have a postInvokeOrionAlertSuppressionGetAlertSuppressionState function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionAlertSuppressionGetAlertSuppressionState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDependenciesRemoveDependencies - errors', () => {
      it('should have a postInvokeOrionDependenciesRemoveDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDependenciesRemoveDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionDeletedAutoDependenciesRemoveIgnoredAutoDependencies - errors', () => {
      it('should have a postInvokeOrionDeletedAutoDependenciesRemoveIgnoredAutoDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionDeletedAutoDependenciesRemoveIgnoredAutoDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEventsAcknowledge - errors', () => {
      it('should have a postInvokeOrionEventsAcknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEventsAcknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesInsertFile - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesInsertFile function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesInsertFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesUpdateFile - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesUpdateFile function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesUpdateFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesLockFile - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesLockFile function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesLockFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesLockFileTable - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesLockFileTable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesLockFileTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesUnlockAllFiles - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesUnlockAllFiles function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesUnlockAllFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesDeleteFile - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesDeleteFile function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesDeleteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionMapStudioFilesGetMapStyle - errors', () => {
      it('should have a postInvokeOrionMapStudioFilesGetMapStyle function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionMapStudioFilesGetMapStyle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNetworkAtlasGetNAVersion - errors', () => {
      it('should have a postInvokeOrionNetworkAtlasGetNAVersion function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNetworkAtlasGetNAVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesUnmanage - errors', () => {
      it('should have a postInvokeOrionNodesUnmanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesUnmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesRemanage - errors', () => {
      it('should have a postInvokeOrionNodesRemanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesRemanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesPollNow - errors', () => {
      it('should have a postInvokeOrionNodesPollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesPollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesGetCountOfElementsPerEngineForLicensing - errors', () => {
      it('should have a postInvokeOrionNodesGetCountOfElementsPerEngineForLicensing function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesGetCountOfElementsPerEngineForLicensing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionNodesCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionNodesCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionNodesCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionNodesCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNodesCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionNodesCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNodesCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionResourcesCheckResourceMigration - errors', () => {
      it('should have a postInvokeOrionResourcesCheckResourceMigration function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionResourcesCheckResourceMigration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionResourcesMigrateClassicToModernResources - errors', () => {
      it('should have a postInvokeOrionResourcesMigrateClassicToModernResources function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionResourcesMigrateClassicToModernResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionResourcesMigrateModernToClassicResources - errors', () => {
      it('should have a postInvokeOrionResourcesMigrateModernToClassicResources function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionResourcesMigrateModernToClassicResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionResourcesGetModernResourceName - errors', () => {
      it('should have a postInvokeOrionResourcesGetModernResourceName function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionResourcesGetModernResourceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVolumesCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVolumesCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVolumesCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVolumesCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionVolumesCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVolumesCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVolumesCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionVolumesCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVolumesCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVolumesCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionVolumesCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVolumesCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVolumesCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVolumesCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVolumesCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentCanInstall - errors', () => {
      it('should have a postInvokeOrionEnvironmentCanInstall function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentCanInstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentAuthorizeWindowsAccountForDatabase - errors', () => {
      it('should have a postInvokeOrionEnvironmentAuthorizeWindowsAccountForDatabase function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentAuthorizeWindowsAccountForDatabase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentGetConnectionString - errors', () => {
      it('should have a postInvokeOrionEnvironmentGetConnectionString function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentGetConnectionString === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentGetSqlServerIpAddresses - errors', () => {
      it('should have a postInvokeOrionEnvironmentGetSqlServerIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentGetSqlServerIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentGetDatabaseAccessCredential - errors', () => {
      it('should have a postInvokeOrionEnvironmentGetDatabaseAccessCredential function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentGetDatabaseAccessCredential === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionEnvironmentGetOrionServerCertificate - errors', () => {
      it('should have a postInvokeOrionEnvironmentGetOrionServerCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionEnvironmentGetOrionServerCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionFrequenciesSaveReportFrequencies - errors', () => {
      it('should have a postInvokeOrionFrequenciesSaveReportFrequencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionFrequenciesSaveReportFrequencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionFrequenciesSaveTimePeriodFrequencies - errors', () => {
      it('should have a postInvokeOrionFrequenciesSaveTimePeriodFrequencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionFrequenciesSaveTimePeriodFrequencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionFrequenciesDeleteFrequencies - errors', () => {
      it('should have a postInvokeOrionFrequenciesDeleteFrequencies function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionFrequenciesDeleteFrequencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionTechnologyPollingAssignmentsEnableAssignments - errors', () => {
      it('should have a postInvokeOrionTechnologyPollingAssignmentsEnableAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionTechnologyPollingAssignmentsEnableAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionTechnologyPollingAssignmentsDisableAssignments - errors', () => {
      it('should have a postInvokeOrionTechnologyPollingAssignmentsDisableAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionTechnologyPollingAssignmentsDisableAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionTechnologyPollingAssignmentsEnableAssignmentsOnNetObjects - errors', () => {
      it('should have a postInvokeOrionTechnologyPollingAssignmentsEnableAssignmentsOnNetObjects function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionTechnologyPollingAssignmentsEnableAssignmentsOnNetObjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionTechnologyPollingAssignmentsDisableAssignmentsOnNetObjects - errors', () => {
      it('should have a postInvokeOrionTechnologyPollingAssignmentsDisableAssignmentsOnNetObjects function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionTechnologyPollingAssignmentsDisableAssignmentsOnNetObjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNotificationItemGroupedAcknowledgeAll - errors', () => {
      it('should have a postInvokeOrionNotificationItemGroupedAcknowledgeAll function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNotificationItemGroupedAcknowledgeAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNotificationItemGroupedAcknowledgeById - errors', () => {
      it('should have a postInvokeOrionNotificationItemGroupedAcknowledgeById function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNotificationItemGroupedAcknowledgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNotificationItemGroupedUnAcknowledgeById - errors', () => {
      it('should have a postInvokeOrionNotificationItemGroupedUnAcknowledgeById function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNotificationItemGroupedUnAcknowledgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNotificationItemGroupedAcknowledgeByType - errors', () => {
      it('should have a postInvokeOrionNotificationItemGroupedAcknowledgeByType function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNotificationItemGroupedAcknowledgeByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionNotificationItemGroupedUnAcknowledgeByType - errors', () => {
      it('should have a postInvokeOrionNotificationItemGroupedUnAcknowledgeByType function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionNotificationItemGroupedUnAcknowledgeByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWebMenuClearCache - errors', () => {
      it('should have a postInvokeOrionWebMenuClearCache function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWebMenuClearCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionRemotingRemoteExecutionPackageExecute - errors', () => {
      it('should have a postInvokeOrionRemotingRemoteExecutionPackageExecute function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionRemotingRemoteExecutionPackageExecute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSCMServerConfigurationPollNow - errors', () => {
      it('should have a postInvokeOrionSCMServerConfigurationPollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSCMServerConfigurationPollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSCMBaselineSetBaseline - errors', () => {
      it('should have a postInvokeOrionSCMBaselineSetBaseline function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSCMBaselineSetBaseline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingsLoad - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingsLoad function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingsLoad === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingsExists - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingsExists function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingsExists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingsSave - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingsSave function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingsSave === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMRecordingCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMRecordingCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMRecordingCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionsUnmanage - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionsUnmanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionsUnmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionsRemanage - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionsRemanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionsRemanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMTransactionCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSEUMTransactionCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMTransactionCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMSettingsGetRecorderCompatibility - errors', () => {
      it('should have a postInvokeOrionSEUMSettingsGetRecorderCompatibility function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMSettingsGetRecorderCompatibility === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSEUMSettingsGetRecorderInstaller - errors', () => {
      it('should have a postInvokeOrionSEUMSettingsGetRecorderInstaller function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSEUMSettingsGetRecorderInstaller === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMSTMIntegrationRefreshLicense - errors', () => {
      it('should have a postInvokeOrionSRMSTMIntegrationRefreshLicense function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMSTMIntegrationRefreshLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMSTMIntegrationUpdateLicense - errors', () => {
      it('should have a postInvokeOrionSRMSTMIntegrationUpdateLicense function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMSTMIntegrationUpdateLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMSTMIntegrationRemoveSTMIntegration - errors', () => {
      it('should have a postInvokeOrionSRMSTMIntegrationRemoveSTMIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMSTMIntegrationRemoveSTMIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArraysGetLicensedArrays - errors', () => {
      it('should have a postInvokeOrionSRMStorageArraysGetLicensedArrays function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArraysGetLicensedArrays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArraysAddSmisCredentials - errors', () => {
      it('should have a postInvokeOrionSRMStorageArraysAddSmisCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArraysAddSmisCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArraysAddExternalProvider - errors', () => {
      it('should have a postInvokeOrionSRMStorageArraysAddExternalProvider function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArraysAddExternalProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArraysAddAllArrays - errors', () => {
      it('should have a postInvokeOrionSRMStorageArraysAddAllArrays function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArraysAddAllArrays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPhysicalDisksGetCountOfElementsPerEngineForLicensing - errors', () => {
      it('should have a postInvokeOrionSRMPhysicalDisksGetCountOfElementsPerEngineForLicensing function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPhysicalDisksGetCountOfElementsPerEngineForLicensing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVServersCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVServersCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVServersCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVServersCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMVServersCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVServersCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVServersCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVServersCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVServersCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVServersCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVServersCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVServersCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVServersCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVServersCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVServersCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMLUNCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMLUNCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMLUNCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMLUNCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMLUNCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMLUNCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMLUNCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMLUNCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMLUNCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMLUNCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMLUNCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMLUNCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMLUNCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMLUNCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMLUNCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVolumeCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVolumeCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVolumeCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVolumeCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMVolumeCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVolumeCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVolumeCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVolumeCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVolumeCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVolumeCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVolumeCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVolumeCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMVolumeCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMVolumeCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMVolumeCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMFileShareCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMFileShareCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMFileShareCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMFileShareCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMFileShareCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMFileShareCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMFileShareCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMFileShareCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMFileShareCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMFileShareCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMFileShareCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMFileShareCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMFileShareCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMFileShareCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMFileShareCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArrayCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMStorageArrayCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArrayCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArrayCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMStorageArrayCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArrayCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMStorageArrayCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMStorageArrayCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMStorageArrayCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMProviderCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMProviderCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMProviderCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMProviderCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMProviderCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMProviderCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMProviderCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMProviderCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMProviderCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMProviderCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMProviderCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMProviderCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMProviderCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMProviderCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMProviderCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPoolCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMPoolCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPoolCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPoolCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionSRMPoolCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPoolCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPoolCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMPoolCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPoolCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPoolCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMPoolCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPoolCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSRMPoolCustomPropertiesValidateCustomProperty - errors', () => {
      it('should have a postInvokeOrionSRMPoolCustomPropertiesValidateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSRMPoolCustomPropertiesValidateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSSOValidateUserToken - errors', () => {
      it('should have a postInvokeOrionSSOValidateUserToken function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSSOValidateUserToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionSysLogAcknowledge - errors', () => {
      it('should have a postInvokeOrionSysLogAcknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionSysLogAcknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionUDTPortAdministrativeShutdown - errors', () => {
      it('should have a postInvokeOrionUDTPortAdministrativeShutdown function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionUDTPortAdministrativeShutdown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionUDTPortAdministrativeEnable - errors', () => {
      it('should have a postInvokeOrionUDTPortAdministrativeEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionUDTPortAdministrativeEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesUnmanage - errors', () => {
      it('should have a postInvokeOrionCloudInstancesUnmanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesUnmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesRemanage - errors', () => {
      it('should have a postInvokeOrionCloudInstancesRemanage function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesRemanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesPollNow - errors', () => {
      it('should have a postInvokeOrionCloudInstancesPollNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesPollNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesStartInstance - errors', () => {
      it('should have a postInvokeOrionCloudInstancesStartInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesStartInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesStopInstance - errors', () => {
      it('should have a postInvokeOrionCloudInstancesStopInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesStopInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesRebootInstance - errors', () => {
      it('should have a postInvokeOrionCloudInstancesRebootInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesRebootInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesDeleteInstance - errors', () => {
      it('should have a postInvokeOrionCloudInstancesDeleteInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesDeleteInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudInstancesDeleteInstanceWithNode - errors', () => {
      it('should have a postInvokeOrionCloudInstancesDeleteInstanceWithNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudInstancesDeleteInstanceWithNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudAwsInstancesForceStopInstance - errors', () => {
      it('should have a postInvokeOrionCloudAwsInstancesForceStopInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudAwsInstancesForceStopInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudAwsInstancesTerminateInstance - errors', () => {
      it('should have a postInvokeOrionCloudAwsInstancesTerminateInstance function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudAwsInstancesTerminateInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionCloudAwsInstancesTerminateInstanceAndRemoveNode - errors', () => {
      it('should have a postInvokeOrionCloudAwsInstancesTerminateInstanceAndRemoveNode function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionCloudAwsInstancesTerminateInstanceAndRemoveNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMClustersCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMClustersCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMClustersCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMClustersCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionVIMClustersCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMClustersCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMClustersCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMClustersCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMClustersCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMClustersCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMClustersCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMClustersCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMVirtualMachinesCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMVirtualMachinesCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMVirtualMachinesCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMVirtualMachinesCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMVirtualMachinesCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMVirtualMachinesCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMHostsCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMHostsCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMHostsCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMHostsCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionVIMHostsCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMHostsCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMHostsCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMHostsCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMHostsCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMHostsCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMHostsCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMHostsCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomPropertyWithValues - errors', () => {
      it('should have a postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomPropertyWithValues function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomPropertyWithValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMDatastoresCustomPropertiesModifyCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMDatastoresCustomPropertiesModifyCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMDatastoresCustomPropertiesModifyCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVIMDatastoresCustomPropertiesDeleteCustomProperty - errors', () => {
      it('should have a postInvokeOrionVIMDatastoresCustomPropertiesDeleteCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVIMDatastoresCustomPropertiesDeleteCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVPNL2LTunnelSetFavorite - errors', () => {
      it('should have a postInvokeOrionVPNL2LTunnelSetFavorite function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVPNL2LTunnelSetFavorite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionVPNL2LTunnelRemoveFavorite - errors', () => {
      it('should have a postInvokeOrionVPNL2LTunnelRemoveFavorite function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionVPNL2LTunnelRemoveFavorite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapInsertMap - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapInsertMap function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapInsertMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapDeleteMap - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapDeleteMap function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapDeleteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapSetMapError - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapSetMapError function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapSetMapError === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapFireMapGenerationIndication - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapFireMapGenerationIndication function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapFireMapGenerationIndication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPollAPSignalStrengthNow - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPollAPSignalStrengthNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPollAPSignalStrengthNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPollRPSignalStrengthNow - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPollRPSignalStrengthNow function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPollRPSignalStrengthNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapStartClientSignalPoll - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapStartClientSignalPoll function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapStartClientSignalPoll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapGetProgress - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapGetProgress function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapGetProgress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapDeleteReferencePoints - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapDeleteReferencePoints function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapDeleteReferencePoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPointInsertMapPoint - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPointInsertMapPoint function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPointInsertMapPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPointDeleteMapPoints - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPointDeleteMapPoints function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPointDeleteMapPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPointDeleteMapPoint - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPointDeleteMapPoint function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPointDeleteMapPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapMapPointSyncMapPoints - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapMapPointSyncMapPoints function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapMapPointSyncMapPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeOrionWirelessHeatMapResourceLimitationInsertResourceLimitation - errors', () => {
      it('should have a postInvokeOrionWirelessHeatMapResourceLimitationInsertResourceLimitation function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeOrionWirelessHeatMapResourceLimitationInsertResourceLimitation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeSystemIndicationReportIndication - errors', () => {
      it('should have a postInvokeSystemIndicationReportIndication function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeSystemIndicationReportIndication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeMetadataEntityGetAliases - errors', () => {
      it('should have a postInvokeMetadataEntityGetAliases function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeMetadataEntityGetAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeMetadataEntityGetSchemaLoadTime - errors', () => {
      it('should have a postInvokeMetadataEntityGetSchemaLoadTime function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeMetadataEntityGetSchemaLoadTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeSystemQueryPlanCacheClear - errors', () => {
      it('should have a postInvokeSystemQueryPlanCacheClear function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeSystemQueryPlanCacheClear === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInvokeSystemActiveQueryCancelByClientSessionID - errors', () => {
      it('should have a postInvokeSystemActiveQueryCancelByClientSessionID function', (done) => {
        try {
          assert.equal(true, typeof a.postInvokeSystemActiveQueryCancelByClientSessionID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreateOrionNodeCustomProperty - errors', () => {
      it('should have a postCreateOrionNodeCustomProperty function', (done) => {
        try {
          assert.equal(true, typeof a.postCreateOrionNodeCustomProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing swisUri', (done) => {
        try {
          a.postCreateOrionNodeCustomProperty(null, null, (data, error) => {
            try {
              const displayE = 'swisUri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-solarwinds-adapter-postCreateOrionNodeCustomProperty', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
