# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the SolarWinds System. The API that was used to build the adapter for SolarWinds is usually available in the report directory of this adapter. The adapter utilizes the SolarWinds API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The SolarWinds adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SolarWinds. With this adapter you have the ability to perform operations such as:

- Performance Monitoring Based on Thresholds and KPIs.
- Add and Remove Device Component to Monitoring

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
