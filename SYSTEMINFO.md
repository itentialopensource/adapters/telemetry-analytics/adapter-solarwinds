# SolarWinds

Vendor: SolarWinds
Homepage: https://www.solarwinds.com/

Product: SolarWinds Information Service (part of SolarWinds Orion)
Product Page: https://www.solarwinds.com/solutions/orion

## Introduction
We classify SolarWinds into the ITSM or Service Management domain as SolarWinds provides functionalities for monitoring and managing IT resources.

## Why Integrate
The SolarWinds adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SolarWinds. With this adapter you have the ability to perform operations such as:

- Performance Monitoring Based on Thresholds and KPIs.
- Add and Remove Device Component to Monitoring

## Additional Product Documentation
The [API documents for SolarWinds](https://documentation.solarwinds.com/en/success_center/sam/content/sam-api-poller-methods.htm)