## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Solarwinds. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Solarwinds.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Solarwinds. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getQuery(query, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postQuery(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUri(uri, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUri(uri, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUri(uri, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBulkUpdate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/BulkUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBulkDelete(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/BulkDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAgentManagementAgent(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AgentManagement.Agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAgentManagementAgentPlugin(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AgentManagement.AgentPlugin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAgentManagementProxy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AgentManagement.Proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAPMNodeToNodeLink(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.APM.NodeToNodeLink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAPMDependencyTcpStatistics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.APM.DependencyTcpStatistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAPMApplicationTcpConnection(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.APM.ApplicationTcpConnection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCirrusIgnoredNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cirrus.IgnoredNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMBaselines(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.Baselines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMBaselineNodeMap(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.BaselineNodeMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMBaselineViolations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.BaselineViolations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMFirmwareOperations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.FirmwareOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMFirmwareOperationNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.FirmwareOperationNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMFirmwareUpgradeImages(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.FirmwareUpgradeImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateNCMFirmwareUpgradeMachineTypes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/NCM.FirmwareUpgradeMachineTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCliDeviceTemplates(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cli.DeviceTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCliDeviceTemplatesNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cli.DeviceTemplatesNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCliCliSessionSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cli.CliSessionSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionCloudAccounts(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Cloud.Accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexSystemPolicy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.System.Policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciApic(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.Apic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciApplicationProfile(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.ApplicationProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciCiscoAciCredential(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.CiscoAciCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciEndpointGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.EndpointGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciFabric(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.Fabric?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciPhysicalEntity(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.PhysicalEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCiscoAciTenant(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.CiscoAci.Tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCmanContainer(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cman.Container?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCmanContainerAgent(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cman.ContainerAgent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCmanContainerEnvironmentVariable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cman.ContainerEnvironmentVariable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCmanContainerHost(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cman.ContainerHost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCmanContainerImage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cman.ContainerImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionSnmpCredentialV2(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.SnmpCredentialV2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionSnmpCredentialV3(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.SnmpCredentialV3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionWindowsCredential(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.WindowsCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionInterface(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionCpu(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Cpu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionPowerControlUnit(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.PowerControlUnit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationAlarm(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.Alarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationCluster(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.Cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationDataCenter(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.DataCenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationDatastore(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.Datastore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationHost(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.Host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationPhysicalDisk(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.PhysicalDisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationPollingTask(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.PollingTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationTriggeredAlarmState(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.TriggeredAlarmState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVCenter(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VCenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVirtualMachine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VirtualMachine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVirtualMachineDisk(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VirtualMachineDisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVMwareCredential(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.VMwareCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVSan(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VSan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVSanDiskGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VSanDiskGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVSanHealthGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VSanHealthGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVSanObjectSpaceSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VSanObjectSpaceSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVirtualizationVSanResyncInfo(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Virtualization.VSanResyncInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionScsiInformation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.ScsiInformation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateCortexOrionVolume(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Cortex.Orion.Volume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPADatabaseInstanceData(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPA.DatabaseInstanceData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPADatabaseInstanceApplicationRelationship(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPA.DatabaseInstanceApplicationRelationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPADatabaseInstanceLun(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPA.DatabaseInstanceLun?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPADpaServer(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPA.DpaServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIApplications(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.Applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIApplicationAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.ApplicationAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIApplicationSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.ApplicationSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIProbes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.Probes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIProbeAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.ProbeAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIProbeSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.ProbeSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDPIProbeProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DPI.ProbeProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionESIIncidentService(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ESI.IncidentService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionHAPools(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.HA.Pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionHAResourcesInstances(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.HA.ResourcesInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionHAPoolMemberInterfacesInfo(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.HA.PoolMemberInterfacesInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNPMInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NPM.Interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateIPAMIPNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/IPAM.IPNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateIPAMSubnet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/IPAM.Subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateIPAMGroupsCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/IPAM.GroupsCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateIPAMNodesCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/IPAM.NodesCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionLicensingLicenseFilters(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Licensing.LicenseFilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathEndpointServices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.EndpointServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathProbes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.Probes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathEndpointServiceAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.EndpointServiceAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathEndpointServiceProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.EndpointServiceProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathThresholds(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.Thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetPathNetworks(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetPath.Networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNPMCustomPollerAssignmentOnNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NPM.CustomPollerAssignmentOnNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNPMCustomPollerAssignmentOnInterface(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NPM.CustomPollerAssignmentOnInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionOLMMessageSources(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.OLM.MessageSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAlerts(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAlertStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AlertStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAlertConfigurations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AlertConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAlertDefinitions(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AlertDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionActions(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionActionsProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ActionsProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionActionAssignmentProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ActionAssignmentProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionActionsAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ActionsAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionActionSchedules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ActionSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAlertSchedules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AlertSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDiscoveryLogs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DiscoveryLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDependencies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionDeletedAutoDependencies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.DeletedAutoDependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionEvents(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionMapStudioFiles(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.MapStudioFiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNodeNotes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NodeNotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionPollers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Pollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionResourceProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ResourceProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionResources(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionVolumes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNodeSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NodeSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionWorldMapPoint(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.WorldMap.Point?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionWorldMapPointLabel(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.WorldMap.PointLabel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNetObjectDowntime(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.NetObjectDowntime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionForecastMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ForecastMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionForecastCapacitySettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.ForecastCapacitySettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionMaintenancePlan(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.MaintenancePlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionMaintenancePlanAssignment(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.MaintenancePlanAssignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionWebFavoriteResource(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Web.FavoriteResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionWebUserWebView(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Web.UserWebView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionOrionServers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.OrionServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionPerfStackProjects(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.PerfStack.Projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionPerfStackStatisticsEntity(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.PerfStack.StatisticsEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionRemotingRemoteExecutionPackage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.Remoting.RemoteExecutionPackage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSamAppOpticsIntegration(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SamAppOptics.Integration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSamAppOpticsRegistrationCountry(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SamAppOptics.RegistrationCountry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSamAppOpticsRegistrationState(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SamAppOptics.RegistrationState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSamAppOpticsApplicationPool(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SamAppOptics.ApplicationPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMServerConfiguration(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.ServerConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMProfiles(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMNodesProfiles(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.NodesProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMNodesProfilesArchive(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.NodesProfilesArchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMNodesProfilesHistory(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.NodesProfilesHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMBaseline(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Baseline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMProfileElements(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.ProfileElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMResultsPolledElements(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Results.PolledElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMResultsPolledElementDetails(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Results.PolledElementDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMResultsNodesPollingErrors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Results.NodesPollingErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMResultsPolledElementErrors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Results.PolledElementErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMResultsElementErrors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.Results.ElementErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSCMPollEntries(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SCM.PollEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSettingOverride(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SettingOverride?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMAgents(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.Agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMRecordings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.Recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMRecordingCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.RecordingCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMRecordingSteps(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.RecordingSteps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMTransactions(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.Transactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMTransactionCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.TransactionCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMTransactionRunParameters(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.TransactionRunParameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMTransactionSteps(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.TransactionSteps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSEUMSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SEUM.Settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionAutoDependencyRoot(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.AutoDependencyRoot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionSSHSession(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.SSH.Session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionVIMClustersCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.VIM.ClustersCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionVIMVirtualMachinesCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.VIM.VirtualMachinesCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionVIMHostsCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.VIM.HostsCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionVIMDatastoresCustomProperties(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.VIM.DatastoresCustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionWirelessHeatMapMap(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/Orion.WirelessHeatMap.Map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateSWISfRemoteSWIS(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/SWISf.RemoteSWIS?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateSystemSubscription(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/System.Subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateSystemSubscriptionProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Create/System.SubscriptionProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventoryPollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventoryEnable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/Enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventoryDisable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/Disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventorySchedulePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/SchedulePollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventoryScheduleEnable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/ScheduleEnable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionADMNodeInventoryUninstallConnectionQualityAgentPlugin(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ADM.NodeInventory/UninstallConnectionQualityAgentPlugin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentDeploy(body, callback)</td>
    <td style="padding:15px">Deploys an agent to a machine defined by hostname and/or IP address.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/Deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentDeployToNode(body, callback)</td>
    <td style="padding:15px">Deploys an agent to an existing node using the supplied credentials.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/DeployToNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentDeployPlugin(body, callback)</td>
    <td style="padding:15px">Deploys the specified plugin to the agent</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/DeployPlugin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentRedeployPlugin(body, callback)</td>
    <td style="padding:15px">Redeploys the specified plugin to the agent</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/RedeployPlugin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentUninstallPlugin(body, callback)</td>
    <td style="padding:15px">Uninstalls the specified plugin from the agent</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/UninstallPlugin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentUninstall(body, callback)</td>
    <td style="padding:15px">Uninstalls the agent.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/Uninstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentDelete(body, callback)</td>
    <td style="padding:15px">Deletes the agent without uninstalling it.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/Delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentApproveReboot(body, callback)</td>
    <td style="padding:15px">Approval for an agent to reboot.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/ApproveReboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentApproveUpdate(body, callback)</td>
    <td style="padding:15px">Approval for an agent to be updated.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/ApproveUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentTestWithEngine(body, callback)</td>
    <td style="padding:15px">Tests the connection between the agent and AMS</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/TestWithEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentAssignToEngine(body, callback)</td>
    <td style="padding:15px">Assigns an agent to a polling engine.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentValidateDeploymentCredentials(body, callback)</td>
    <td style="padding:15px">Validates if provided credentials are valid for agent deployment. If credentials pass validation they can be safely used for deployment via Deploy verb.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/ValidateDeploymentCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAgentManagementAgentRestartAgent(body, callback)</td>
    <td style="padding:15px">Initiate Orion Agent service restart.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AgentManagement.Agent/RestartAgent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISSiteStart(body, callback)</td>
    <td style="padding:15px">Start IIS site.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.Site/Start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISSiteStop(body, callback)</td>
    <td style="padding:15px">Stop IIS site.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.Site/Stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISSiteRestart(body, callback)</td>
    <td style="padding:15px">Restart IIS site.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.Site/Restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISApplicationPoolStart(body, callback)</td>
    <td style="padding:15px">Start IIS application pool.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.ApplicationPool/Start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISApplicationPoolStop(body, callback)</td>
    <td style="padding:15px">Stop IIS application pool.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.ApplicationPool/Stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMIISApplicationPoolRestart(body, callback)</td>
    <td style="padding:15px">Restart IIS application pool.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.IIS.ApplicationPool/Restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationUnmanage(body, callback)</td>
    <td style="padding:15px">Unmanage existed application.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.Application/Unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationRemanage(body, callback)</td>
    <td style="padding:15px">Remanage existed application.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.Application/Remanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationCreateApplication(body, callback)</td>
    <td style="padding:15px">Create new application.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.Application/CreateApplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationDeleteApplication(body, callback)</td>
    <td style="padding:15px">Delete existed application.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.Application/DeleteApplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationPollNow(body, callback)</td>
    <td style="padding:15px">Poll existed application.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.Application/PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">Create application custom property.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">Create application custom property with values.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">Modify application custom property.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">Delete application custom property.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationTemplateUpdateApplicationTemplateSettings(body, callback)</td>
    <td style="padding:15px">Update application template settings.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationTemplate/UpdateApplicationTemplateSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMApplicationTemplateDeleteTemplate(body, callback)</td>
    <td style="padding:15px">Delete existed application template.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ApplicationTemplate/DeleteTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMServerManagementStartService(body, callback)</td>
    <td style="padding:15px">Start windows service.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ServerManagement/StartService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMServerManagementStopService(body, callback)</td>
    <td style="padding:15px">Stop windows service.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ServerManagement/StopService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMServerManagementRestartService(body, callback)</td>
    <td style="padding:15px">Restart windows service.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ServerManagement/RestartService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAPMServerManagementRebootNode(body, callback)</td>
    <td style="padding:15px">Restart the node.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.APM.ServerManagement/RebootNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionStacksRelationTraverse(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Stacks.Relation/Traverse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionStacksRelationProcessUi(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Stacks.Relation/ProcessUi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionASANodeExecuteCliCommand(body, callback)</td>
    <td style="padding:15px">Execute CLI command using SSH protocol on port 22</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ASA.Node/ExecuteCliCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionASAInterfacesSetFavorite(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ASA.Interfaces/SetFavorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionASAInterfacesRemoveFavorite(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ASA.Interfaces/RemoveFavorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsGetJob(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/GetJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsUpdateJob(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/UpdateJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsAddJob(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/AddJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsDeleteJobs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/DeleteJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsEnableOrDisableJobs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/EnableOrDisableJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsClearJobLog(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/ClearJobLog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsGetJobStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/GetJobStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusNCMNCMJobsGetJobLog(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.NCM_NCMJobs/GetJobLog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusInventoryQueueStartInventory(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.InventoryQueue/StartInventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusInventoryQueueReportError(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.InventoryQueue/ReportError?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusInventoryQueueCancelInventory(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.InventoryQueue/CancelInventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusInventoryQueueClearInventory(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.InventoryQueue/ClearInventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMVulnerabilitiesAnnouncementsStartVulnerabilityMatching(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.VulnerabilitiesAnnouncements/StartVulnerabilityMatching?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMVulnerabilitiesAnnouncementsIsVulnerabilityMatchingActive(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.VulnerabilitiesAnnouncements/IsVulnerabilityMatchingActive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMVulnerabilitiesAnnouncementsInitVulnerabilitySchedule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.VulnerabilitiesAnnouncements/InitVulnerabilitySchedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsAddSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/AddSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsGetSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/GetSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsUpdateSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/UpdateSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsDeleteSnippets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/DeleteSnippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsImportSnippets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/ImportSnippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsCopySnippets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/CopySnippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsGetTagsList(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/GetTagsList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsGetTagsListForSnippets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/GetTagsListForSnippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsAddTags(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/AddTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsDeleteTags(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/DeleteTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigSnippetsSaveSnippetAsCopy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigSnippets/SaveSnippetAsCopy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveDownload(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/Download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveGetStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/GetStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveGetStatusWithLimitations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/GetStatusWithLimitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveUpload(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/Upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveExecute(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/Execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveCancelTransfer(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/CancelTransfer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveCancelTransferWithLimitations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/CancelTransferWithLimitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearComplete(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearComplete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearCompleteWithLimitations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearCompleteWithLimitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearErrors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearErrorsWithLimitations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearErrorsWithLimitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearAll(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearAllWithLimitations(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearAllWithLimitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveGetPermissionsByRole(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/GetPermissionsByRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveDeleteConfigs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/DeleteConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveGetConfigTypes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/GetConfigTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveReportError(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ReportError?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveCancelTransfers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/CancelTransfers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveClearTransfers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ClearTransfers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveExecuteScript(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ExecuteScript?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveUploadConfig(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/UploadConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveDownloadConfig(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/DownloadConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveExecuteScriptOnNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ExecuteScriptOnNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveDownloadConfigOnNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/DownloadConfigOnNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveConfigSearch(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ConfigSearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveRunIndexOptimization(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/RunIndexOptimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveUpdateConfig(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/UpdateConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveCloneConfig(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/CloneConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveImportConfig(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ImportConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveSetClearBaseline(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/SetClearBaseline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveValidateBinaryConfigStorage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/ValidateBinaryConfigStorage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveSearchInterfaceConfigSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/SearchInterfaceConfigSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusConfigArchiveGetInterfaceConfigSnippets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.ConfigArchive/GetInterfaceConfigSnippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusSnippetArchiveAddSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.SnippetArchive/AddSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusSnippetArchiveDeleteSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.SnippetArchive/DeleteSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusSnippetArchiveUpdateSnippet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.SnippetArchive/UpdateSnippet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsTestRule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/TestRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPolicyReport(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPolicyReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsUpdatePolicyReport(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/UpdatePolicyReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsAddPolicyReport(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/AddPolicyReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsDeletePolicyReports(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/DeletePolicyReports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPolicy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsUpdatePolicy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/UpdatePolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsAddPolicy(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/AddPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsDeletePolicies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/DeletePolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPolicyRule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPolicyRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsUpdatePolicyRule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/UpdatePolicyRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsAddPolicyRule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/AddPolicyRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsDeletePolicyRules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/DeletePolicyRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsStartCaching(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/StartCaching?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsUpdateReportStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/UpdateReportStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPagablePoliciesList(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPagablePoliciesList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPoliciesRowCount(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPoliciesRowCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPagablePolicyRulesList(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPagablePolicyRulesList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetPolicyRulesRowCount(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetPolicyRulesRowCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetComplianceDataTable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetComplianceDataTable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetComplianceColumnsInJSON(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetComplianceColumnsInJSON?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGetRowNumber(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GetRowNumber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusPolicyReportsGenerateRemediationScriptForNodes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.PolicyReports/GenerateRemediationScriptForNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMEosBeginRefreshAll(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.Eos/BeginRefreshAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMEosRefreshNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.Eos/RefreshNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMEosIsRefreshingAll(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.Eos/IsRefreshingAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMEosInitSchedule(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.Eos/InitSchedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareStorageValidateFirmwareStorage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareStorage/ValidateFirmwareStorage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareStorageDeleteFirmwareImages(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareStorage/DeleteFirmwareImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareDefinitionsGetFirmwareDefinition(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareDefinitions/GetFirmwareDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareDefinitionsAddFirmwareDefinition(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareDefinitions/AddFirmwareDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareDefinitionsUpdateFirmwareDefinition(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareDefinitions/UpdateFirmwareDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareDefinitionsDeleteFirmwareDefinitions(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareDefinitions/DeleteFirmwareDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsPrepareFirmwareUpgrade(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/PrepareFirmwareUpgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsPrepareRollBack(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/PrepareRollBack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsPrepareReExecuteFailed(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/PrepareReExecuteFailed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsStartUpgrade(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/StartUpgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsCancelUpgrade(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/CancelUpgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeNCMFirmwareOperationsGenerateScriptPreview(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/NCM.FirmwareOperations/GenerateScriptPreview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusReportsRun(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.Reports/Run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusReportsRunPagable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.Reports/RunPagable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusReportsGetRecordsCount(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.Reports/GetRecordsCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCirrusReportsGetDiplayableColumns(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cirrus.Reports/GetDiplayableColumns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionGroupCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.GroupCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionGroupCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.GroupCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionGroupCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.GroupCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionGroupCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.GroupCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionGroupCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.GroupCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionContainerMemberDefinitionGetMembers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ContainerMemberDefinition/GetMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionContainerMemberDefinitionGetFirstNMembers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ContainerMemberDefinition/GetFirstNMembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexManagementPublishEvent(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Management/PublishEvent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciAssignAciPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.AssignAciPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciGetPollInterval(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.GetPollInterval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciIsAciPollingAssigned(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.IsAciPollingAssigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciSyncAciCredentialsWithOrion(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.SyncAciCredentialsWithOrion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciTestAciCredentials(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.TestAciCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApicOrionCiscoAciUnassignAciPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Apic/Orion.CiscoAci.UnassignAciPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciApplicationProfileCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.ApplicationProfile/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciEndpointGroupCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.EndpointGroup/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciFabricCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Fabric/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciPhysicalEntityCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.PhysicalEntity/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCiscoAciTenantCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.CiscoAci.Tenant/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.Container/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerAgentCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerAgent/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerEnvironmentVariableCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerEnvironmentVariable/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerHostCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerHost/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionCmanContainerImageCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Cman.ContainerImage/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionMonitoringElementCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.MonitoringElement/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreAddToCortex(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.AddToCortex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionInterfaceCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Interface/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreAddToCortex(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.AddToCortex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionNodeCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Node/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionPowerControlUnitCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.PowerControlUnit/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationClusterCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Cluster/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDataCenterCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.DataCenter/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationDatastoreCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Datastore/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHostCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.Host/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationHypervisorEntityCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.HypervisorEntity/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVCenterCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VCenter/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachine/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVirtualMachineDiskCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VirtualMachineDisk/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVirtualizationVSanCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Virtualization.VSan/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreAddToCortex(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.AddToCortex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreAssignToEngine(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.AssignToEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreGetSupportedMetrics(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.GetSupportedMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreInventoryNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.InventoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCorePollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreSetPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.SetPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreStartRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.StartRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeCortexOrionVolumeCoreStopRealTimePolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Cortex.Orion.Volume/Core.StopRealTimePolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPADpaServerRefreshSchema(body, callback)</td>
    <td style="padding:15px">Refresh federation schema of for particular DPA Server</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPA.DpaServer/RefreshSchema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPIProbesReloadProbeSettings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPI.Probes/ReloadProbeSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPIProbesReloadAppDefinitions(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPI.Probes/ReloadAppDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPIProbesGetProbeCapabilities(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPI.Probes/GetProbeCapabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPIProbesDeployLocalTrafficProbe(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPI.Probes/DeployLocalTrafficProbe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDPIProbesDeploySpanPortProbe(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DPI.Probes/DeploySpanPortProbe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionESIIncidentIntegrationSetIncidentIntegrationState(body, callback)</td>
    <td style="padding:15px">Sets the state of incident integration respectd by Orion web UI. True is enabled, false means disabled.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.ESI.IncidentIntegration/SetIncidentIntegrationState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionF5SystemDeviceTestApiPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.F5.System.Device/TestApiPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionF5SystemDeviceEnableApiPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.F5.System.Device/EnableApiPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionF5SystemDeviceDisableApiPolling(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.F5.System.Device/DisableApiPolling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionF5LTMServerLinkNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.F5.LTM.Server/LinkNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionF5LTMServerUnlinkNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.F5.LTM.Server/UnlinkNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareInfoBaseEnableHardwareHealth(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareInfoBase/EnableHardwareHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareInfoBaseDisableHardwareHealth(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareInfoBase/DisableHardwareHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareInfoBaseDeleteHardwareHealth(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareInfoBase/DeleteHardwareHealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareInfoBaseIsHardwareHealthEnabled(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareInfoBase/IsHardwareHealthEnabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareItemBaseEnableSensors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareItemBase/EnableSensors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareItemBaseDisableSensors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareItemBase/DisableSensors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareItemThresholdSetThreshold(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareItemThreshold/SetThreshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHardwareHealthHardwareItemThresholdClearThresholds(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HardwareHealth.HardwareItemThreshold/ClearThresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsCreatePool(body, callback)</td>
    <td style="padding:15px">Creates pool based on provided members and resource parameters</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/CreatePool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsEditPool(body, callback)</td>
    <td style="padding:15px">Updates pool with a given poolId</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/EditPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsValidateCreatePool(body, callback)</td>
    <td style="padding:15px">Validates pool (without creating it) based on provided members and resource parameters</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/ValidateCreatePool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsValidateEditPool(body, callback)</td>
    <td style="padding:15px">Validates pool with given poolId and resource parameters (without actual update)</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/ValidateEditPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsEnablePool(body, callback)</td>
    <td style="padding:15px">Enables pool with a given poolId</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/EnablePool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsDisablePool(body, callback)</td>
    <td style="padding:15px">Disables pool with a given poolId</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/DisablePool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsDeletePool(body, callback)</td>
    <td style="padding:15px">Delete pool with given poolId.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/DeletePool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsSwitchover(body, callback)</td>
    <td style="padding:15px">Manual failover on a given pool.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/Switchover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionHAPoolsDeleteStaleEngine(body, callback)</td>
    <td style="padding:15px">Deletes OrionServer and related pool memeber with a given hostName.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.HA.Pools/DeleteStaleEngine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesSetPowerLevel(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/SetPowerLevel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesUnmanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/Unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesRemanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/Remanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesDiscoverInterfacesOnNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/DiscoverInterfacesOnNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesAddInterfacesOnNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/AddInterfacesOnNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesCreateInterfacesPluginConfiguration(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.Interfaces/CreateInterfacesPluginConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.InterfacesCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.InterfacesCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.InterfacesCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNPMInterfacesCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NPM.InterfacesCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementChangeIpStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/ChangeIpStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementChangeIpStatusForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/ChangeIpStatusForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementGetFirstAvailableIp(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/GetFirstAvailableIp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementGetFirstAvailableIpForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/GetFirstAvailableIpForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementStartIpReservation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/StartIpReservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementStartIpReservationForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/StartIpReservationForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCancelIpReservation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CancelIpReservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCancelIpReservationForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CancelIpReservationForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementFinishIpReservation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/FinishIpReservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementFinishIpReservationForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/FinishIpReservationForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCreateSubnet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CreateSubnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCreateSubnetForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CreateSubnetForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCreateIPv6Subnet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CreateIPv6Subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementCreateIPv6SubnetForGroup(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/CreateIPv6SubnetForGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMSubnetManagementChangeDisableAutoScanning(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.SubnetManagement/ChangeDisableAutoScanning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMDhcpDnsManagementCreateIpReservation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.DhcpDnsManagement/CreateIpReservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMDhcpDnsManagementRemoveIpReservation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.DhcpDnsManagement/RemoveIpReservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMDhcpDnsManagementGetAandPTRrecordsForDnsZone(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.DhcpDnsManagement/GetAandPTRrecordsForDnsZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementAddDnsARecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/AddDnsARecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementChangeDnsARecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/ChangeDnsARecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementRemoveDnsARecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/RemoveDnsARecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementAddDnsAaaaRecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/AddDnsAaaaRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementChangeDnsAaaaRecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/ChangeDnsAaaaRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementRemoveDnsAaaaRecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/RemoveDnsAaaaRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementAddDnsARecordWithPtr(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/AddDnsARecordWithPtr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementAddPtrToDnsARecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/AddPtrToDnsARecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementAddPtrRecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/AddPtrRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMIPAddressManagementRemovePtrRecord(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.IPAddressManagement/RemovePtrRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMAttrDefineAddCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.AttrDefine/AddCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMAttrDefineUpdateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.AttrDefine/UpdateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMAttrDefineDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.AttrDefine/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMGroupsCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.GroupsCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMGroupsCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.GroupsCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMGroupsCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.GroupsCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMGroupsCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.GroupsCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMNodesCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.NodesCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMNodesCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.NodesCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMNodesCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.NodesCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeIPAMNodesCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/IPAM.NodesCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesActivateOnline(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/ActivateOnline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesActivateOffline(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/ActivateOffline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesDeactivate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/Deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesReAssignExactlyTo(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/ReAssignExactlyTo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesUnAssignFromAllServers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/UnAssignFromAllServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesFindValidAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/FindValidAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesGetEvaluationState(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/GetEvaluationState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionLicensingLicensesGetAvailableAssignments(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Licensing.Licenses/GetAvailableAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNetflowSourceSetManualSamplingRate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Netflow.Source/SetManualSamplingRate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNetflowSourceSetAutoDetectedSamplingRate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Netflow.Source/SetAutoDetectedSamplingRate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionOLMLogEntryUidMaxForDate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.OLM.LogEntry/UidMaxForDate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionOLMLogEntryUidMinForDate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.OLM.LogEntry/UidMinForDate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionOLMLogEntryUidExtractDate(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.OLM.LogEntry/UidExtractDate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertActiveAcknowledge(body, callback)</td>
    <td style="padding:15px">Acknowledge active alerts, based on array of alert active ids and desired notes.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertActive/Acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertActiveUnacknowledge(body, callback)</td>
    <td style="padding:15px">Unacknowledge active alerts, based on array of alert active ids.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertActive/Unacknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertActiveClearAlert(body, callback)</td>
    <td style="padding:15px">Delete active alert from database. Manual alert reset</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertActive/ClearAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertActiveAppendNote(body, callback)</td>
    <td style="padding:15px">Appends note to Alert object.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertActive/AppendNote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertsMigrateAllBasicAlerts(body, callback)</td>
    <td style="padding:15px">This verb migrates all alerts created in Basic Alerts Manager to new data entities used in Alert Manager on Orion website.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Alerts/MigrateAllBasicAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertsMigrateBasicAlert(body, callback)</td>
    <td style="padding:15px">This verb migrates alert created in Basic Alerts Manager to new data entities used in Alert Manager on Orion website.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Alerts/MigrateBasicAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertStatusAcknowledge(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertStatus/Acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertStatusAcknowledgeAlert(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertStatus/AcknowledgeAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertStatusAddNote(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertStatus/AddNote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsMigrateAllAdvancedAlerts(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurations/MigrateAllAdvancedAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsMigrateAdvancedAlert(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurations/MigrateAdvancedAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsMigrateAdvancedAlertFromXML(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurations/MigrateAdvancedAlertFromXML?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsImport(body, callback)</td>
    <td style="padding:15px">This verb imports alert into system from alert xml</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurations/Import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsExport(body, callback)</td>
    <td style="padding:15px">This verb exports alert definition</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurations/Export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurationsCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurationsCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurationsCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurationsCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertConfigurationsCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertConfigurationsCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertDefinitionsRevertMigratedAlert(body, callback)</td>
    <td style="padding:15px">Sets alert definition Reverted property to true with means that alert will be processed by alerting service v1. Also sets Enable property based on parameter.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertDefinitions/RevertMigratedAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsDeleteActionsByAssignments(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/DeleteActionsByAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsDeleteActionsByAssignmentsAndCategory(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/DeleteActionsByAssignmentsAndCategory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsSaveActionsForAssignments(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/SaveActionsForAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsUpdateAction(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/UpdateAction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsUpdateActionsProperties(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/UpdateActionsProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsUpdateActionsDescriptions(body, callback)</td>
    <td style="padding:15px">This verb updates actions descriptions after updating of actions properties.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/UpdateActionsDescriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsUpdateActionsFrequencies(body, callback)</td>
    <td style="padding:15px">This verb updates actions frequencies after multi editing of actions</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/UpdateActionsFrequencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsTestAlertingAction(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/TestAlertingAction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionActionsTestReportingAction(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Actions/TestReportingAction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertSuppressionSuppressAlerts(body, callback)</td>
    <td style="padding:15px">Do not trigger any alerts for entities defined in entityUris array during the suppressFrom-suppressUntil time period.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertSuppression/SuppressAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertSuppressionResumeAlerts(body, callback)</td>
    <td style="padding:15px">Alerts for entities defined in entityUris array will be triggered as usual.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertSuppression/ResumeAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionAlertSuppressionGetAlertSuppressionState(body, callback)</td>
    <td style="padding:15px">Get Alert Suppression State for provided list of entities.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.AlertSuppression/GetAlertSuppressionState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDependenciesRemoveDependencies(body, callback)</td>
    <td style="padding:15px">Ignore dependencies. Such dependencies are ingored in Autodependency calculation.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Dependencies/RemoveDependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionDeletedAutoDependenciesRemoveIgnoredAutoDependencies(body, callback)</td>
    <td style="padding:15px">Removes ignored dependencies.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.DeletedAutoDependencies/RemoveIgnoredAutoDependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEventsAcknowledge(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Events/Acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesInsertFile(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/InsertFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesUpdateFile(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/UpdateFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesLockFile(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/LockFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesLockFileTable(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/LockFileTable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesUnlockAllFiles(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/UnlockAllFiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesDeleteFile(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/DeleteFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionMapStudioFilesGetMapStyle(body, callback)</td>
    <td style="padding:15px">Get map style of the map</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.MapStudioFiles/GetMapStyle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNetworkAtlasGetNAVersion(body, callback)</td>
    <td style="padding:15px">Returns version of the installed Network Atlas.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NetworkAtlas/GetNAVersion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesUnmanage(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Nodes/Unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesRemanage(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Nodes/Remanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesPollNow(body, callback)</td>
    <td style="padding:15px">It will poll node instance and update its information</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Nodes/PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesGetCountOfElementsPerEngineForLicensing(body, callback)</td>
    <td style="padding:15px">Returns count of used elements (per engine) for licensing</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Nodes/GetCountOfElementsPerEngineForLicensing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NodesCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NodesCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NodesCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NodesCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNodesCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NodesCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionResourcesCheckResourceMigration(body, callback)</td>
    <td style="padding:15px">This verb checks, whether it is possible to migrate classic resources (charts) to its modern version.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Resources/CheckResourceMigration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionResourcesMigrateClassicToModernResources(body, callback)</td>
    <td style="padding:15px">This verb migrates classic resources (charts) to its modern version.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Resources/MigrateClassicToModernResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionResourcesMigrateModernToClassicResources(body, callback)</td>
    <td style="padding:15px">This verb reverts migration back to classic resources (charts).</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Resources/MigrateModernToClassicResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionResourcesGetModernResourceName(body, callback)</td>
    <td style="padding:15px">Returns new apollo chart name for given classic chart name</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Resources/GetModernResourceName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVolumesCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VolumesCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVolumesCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VolumesCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVolumesCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VolumesCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVolumesCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VolumesCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVolumesCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VolumesCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentCanInstall(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/CanInstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentAuthorizeWindowsAccountForDatabase(body, callback)</td>
    <td style="padding:15px">Adds provided user to Orion database with db_owner permissions.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/AuthorizeWindowsAccountForDatabase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentGetConnectionString(body, callback)</td>
    <td style="padding:15px">Returns connection string</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/GetConnectionString?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentGetSqlServerIpAddresses(body, callback)</td>
    <td style="padding:15px">Returns array of IP addresses of current SQL server</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/GetSqlServerIpAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentGetDatabaseAccessCredential(body, callback)</td>
    <td style="padding:15px">Returns credential used to access Orion database when system is configured to use Windows authentication</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/GetDatabaseAccessCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionEnvironmentGetOrionServerCertificate(body, callback)</td>
    <td style="padding:15px">Returns Orion certificate</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Environment/GetOrionServerCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionFrequenciesSaveReportFrequencies(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Frequencies/SaveReportFrequencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionFrequenciesSaveTimePeriodFrequencies(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Frequencies/SaveTimePeriodFrequencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionFrequenciesDeleteFrequencies(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Frequencies/DeleteFrequencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionTechnologyPollingAssignmentsEnableAssignments(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.TechnologyPollingAssignments/EnableAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionTechnologyPollingAssignmentsDisableAssignments(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.TechnologyPollingAssignments/DisableAssignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionTechnologyPollingAssignmentsEnableAssignmentsOnNetObjects(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.TechnologyPollingAssignments/EnableAssignmentsOnNetObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionTechnologyPollingAssignmentsDisableAssignmentsOnNetObjects(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.TechnologyPollingAssignments/DisableAssignmentsOnNetObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNotificationItemGroupedAcknowledgeAll(body, callback)</td>
    <td style="padding:15px">Sets notification item acknowledgement timestamp and user for all items.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NotificationItemGrouped/AcknowledgeAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNotificationItemGroupedAcknowledgeById(body, callback)</td>
    <td style="padding:15px">Sets notification item acknowledgement timestamp and user for specific item.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NotificationItemGrouped/AcknowledgeById?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNotificationItemGroupedUnAcknowledgeById(body, callback)</td>
    <td style="padding:15px">Resets notification item acknowledgement for specific item.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NotificationItemGrouped/UnAcknowledgeById?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNotificationItemGroupedAcknowledgeByType(body, callback)</td>
    <td style="padding:15px">Sets notification item acknowledgement timestamp and user for items of a specific type.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NotificationItemGrouped/AcknowledgeByType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionNotificationItemGroupedUnAcknowledgeByType(body, callback)</td>
    <td style="padding:15px">Resets notification item acknowledgement for items of a specific type.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.NotificationItemGrouped/UnAcknowledgeByType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWebMenuClearCache(body, callback)</td>
    <td style="padding:15px">Clears menu item cache.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Web.Menu/ClearCache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionRemotingRemoteExecutionPackageExecute(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Remoting.RemoteExecutionPackage/Execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSCMServerConfigurationPollNow(body, callback)</td>
    <td style="padding:15px">On target nodes triggers refreshing of watchers and executes jobs for polling SWIS elements.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SCM.ServerConfiguration/PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSCMBaselineSetBaseline(body, callback)</td>
    <td style="padding:15px">Create or update baseline and snapshot all related data so that they are not touched by maintenance processes.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SCM.Baseline/SetBaseline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingsLoad(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Recordings/Load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingsExists(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Recordings/Exists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingsSave(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Recordings/Save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.RecordingCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.RecordingCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.RecordingCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMRecordingCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.RecordingCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionsUnmanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Transactions/Unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionsRemanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Transactions/Remanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.TransactionCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.TransactionCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.TransactionCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMTransactionCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.TransactionCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMSettingsGetRecorderCompatibility(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Settings/GetRecorderCompatibility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSEUMSettingsGetRecorderInstaller(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SEUM.Settings/GetRecorderInstaller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMSTMIntegrationRefreshLicense(body, callback)</td>
    <td style="padding:15px">Triggers refresh license (called by STM)</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.STMIntegration/RefreshLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMSTMIntegrationUpdateLicense(body, callback)</td>
    <td style="padding:15px">Triggers update license (called by STM)</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.STMIntegration/UpdateLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMSTMIntegrationRemoveSTMIntegration(body, callback)</td>
    <td style="padding:15px">Disables STM integration and renews the latest known license before integration (called by STM)</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.STMIntegration/RemoveSTMIntegration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArraysGetLicensedArrays(body, callback)</td>
    <td style="padding:15px">Will return list of Storage Array IDs which are licensed.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrays/GetLicensedArrays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArraysAddSmisCredentials(body, callback)</td>
    <td style="padding:15px">Adds smis credentials. Returns credentials ID</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrays/AddSmisCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArraysAddExternalProvider(body, callback)</td>
    <td style="padding:15px">Adds external provider. Receives credential ID, returns provider ID.</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrays/AddExternalProvider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArraysAddAllArrays(body, callback)</td>
    <td style="padding:15px">Adds all arrays within given provider. Returns true if success</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrays/AddAllArrays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPhysicalDisksGetCountOfElementsPerEngineForLicensing(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PhysicalDisks/GetCountOfElementsPerEngineForLicensing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVServersCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VServersCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVServersCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VServersCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVServersCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VServersCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVServersCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VServersCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVServersCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VServersCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMLUNCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.LUNCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMLUNCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.LUNCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMLUNCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.LUNCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMLUNCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.LUNCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMLUNCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.LUNCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVolumeCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VolumeCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVolumeCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VolumeCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVolumeCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VolumeCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVolumeCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VolumeCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMVolumeCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.VolumeCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMFileShareCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.FileShareCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMFileShareCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.FileShareCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMFileShareCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.FileShareCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMFileShareCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.FileShareCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMFileShareCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.FileShareCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrayCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArrayCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrayCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArrayCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrayCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArrayCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrayCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMStorageArrayCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.StorageArrayCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMProviderCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.ProviderCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMProviderCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.ProviderCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMProviderCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.ProviderCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMProviderCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.ProviderCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMProviderCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.ProviderCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPoolCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PoolCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPoolCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PoolCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPoolCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PoolCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPoolCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PoolCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSRMPoolCustomPropertiesValidateCustomProperty(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SRM.PoolCustomProperties/ValidateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSSOValidateUserToken(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SSO/ValidateUserToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionSysLogAcknowledge(body, callback)</td>
    <td style="padding:15px">ToDo</td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.SysLog/Acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionUDTPortAdministrativeShutdown(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.UDT.Port/AdministrativeShutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionUDTPortAdministrativeEnable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.UDT.Port/AdministrativeEnable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesUnmanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/Unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesRemanage(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/Remanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesPollNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/PollNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesStartInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/StartInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesStopInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/StopInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesRebootInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/RebootInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesDeleteInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/DeleteInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudInstancesDeleteInstanceWithNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Instances/DeleteInstanceWithNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudAwsInstancesForceStopInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Aws.Instances/ForceStopInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudAwsInstancesTerminateInstance(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Aws.Instances/TerminateInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionCloudAwsInstancesTerminateInstanceAndRemoveNode(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.Cloud.Aws.Instances/TerminateInstanceAndRemoveNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMClustersCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.ClustersCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMClustersCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.ClustersCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMClustersCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.ClustersCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMClustersCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.ClustersCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.VirtualMachinesCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMVirtualMachinesCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.VirtualMachinesCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMVirtualMachinesCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.VirtualMachinesCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMVirtualMachinesCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.VirtualMachinesCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMHostsCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.HostsCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMHostsCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.HostsCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMHostsCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.HostsCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMHostsCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.HostsCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.DatastoresCustomProperties/CreateCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMDatastoresCustomPropertiesCreateCustomPropertyWithValues(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.DatastoresCustomProperties/CreateCustomPropertyWithValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMDatastoresCustomPropertiesModifyCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.DatastoresCustomProperties/ModifyCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVIMDatastoresCustomPropertiesDeleteCustomProperty(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VIM.DatastoresCustomProperties/DeleteCustomProperty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVPNL2LTunnelSetFavorite(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VPN.L2LTunnel/SetFavorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionVPNL2LTunnelRemoveFavorite(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.VPN.L2LTunnel/RemoveFavorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapInsertMap(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/InsertMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapDeleteMap(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/DeleteMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapSetMapError(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/SetMapError?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapFireMapGenerationIndication(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/FireMapGenerationIndication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPollAPSignalStrengthNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/PollAPSignalStrengthNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPollRPSignalStrengthNow(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/PollRPSignalStrengthNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapStartClientSignalPoll(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/StartClientSignalPoll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapGetProgress(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/GetProgress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapDeleteReferencePoints(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.Map/DeleteReferencePoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPointInsertMapPoint(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.MapPoint/InsertMapPoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPointDeleteMapPoints(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.MapPoint/DeleteMapPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPointDeleteMapPoint(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.MapPoint/DeleteMapPoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapMapPointSyncMapPoints(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.MapPoint/SyncMapPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeOrionWirelessHeatMapResourceLimitationInsertResourceLimitation(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Orion.WirelessHeatMap.ResourceLimitation/InsertResourceLimitation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeSystemIndicationReportIndication(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/System.Indication/ReportIndication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeMetadataEntityGetAliases(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Metadata.Entity/GetAliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeMetadataEntityGetSchemaLoadTime(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/Metadata.Entity/GetSchemaLoadTime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeSystemQueryPlanCacheClear(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/System.QueryPlanCache/Clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInvokeSystemActiveQueryCancelByClientSessionID(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Invoke/System.ActiveQuery/CancelByClientSessionID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreateOrionNodeCustomProperty(swisUri, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/CustomProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
